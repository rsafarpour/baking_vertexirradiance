#define NOMINMAX

#include "FileIO.h"
#include <stdio.h>
#include <Windows.h>

U64 PokeFilesize(const char* filename)
{
    FILE* f = nullptr;
    errno_t err = fopen_s(&f, filename, "r");

    if (err == 0)
    {
        if(fseek(f, 0, SEEK_END) == 0)
        {
            long fsize = ftell(f);
            if (fsize != EOF)
            { return fsize; }
        }
        fclose(f);
    }

    OutputDebugStringA("PokeFilesize: Error accessing file: \"");
    OutputDebugStringA(filename);
    OutputDebugStringA("\"\n");
    return 0;
}

U64 ReadFile(const char* filename, char* dst, U64 dst_size)
{
    FILE* f = nullptr;
    errno_t err = fopen_s(&f, filename, "r");
    if (err != 0)
    { 
        OutputDebugStringA("PokeFilesize: Error opening file: \"");
        OutputDebugStringA(filename);
        OutputDebugStringA("\"\n");
        return 0; 
    }

    U64 end = fread(dst, sizeof(char), (size_t)dst_size, f);
    if (end <= dst_size)
    {
        dst[end] = '\0';
    }

    fclose(f);
    return end;
}