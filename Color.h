#pragma once

#include <xmmintrin.h>
#include <immintrin.h>

#include "Vector3.h"

using ColorRGBA_F = __m128;

inline ColorRGBA_F bary_interpolate(const Vector3 coords, ColorRGBA_F a, ColorRGBA_F b, ColorRGBA_F c)
{
    __m128 tmp = _mm_setzero_ps();

    _MM_TRANSPOSE4_PS(a, b, c, tmp);
    __m128 red   = _mm_mul_ps(coords, a);
    __m128 green = _mm_mul_ps(coords, b);
    __m128 blue  = _mm_mul_ps(coords, c);
    __m128 alpha = _mm_mul_ps(coords, tmp);


    red   = _mm_hadd_ps(red, red);
    red   = _mm_hadd_ps(red, red);
    green = _mm_hadd_ps(green, green);
    green = _mm_hadd_ps(green, green);
    blue  = _mm_hadd_ps(blue, blue);
    blue  = _mm_hadd_ps(blue, blue);
    alpha = _mm_hadd_ps(alpha, alpha);
    alpha = _mm_hadd_ps(alpha, alpha);
    _MM_TRANSPOSE4_PS(red, green, blue, alpha);

    return red;
}

inline ColorRGBA_F alpha_mul(const ColorRGBA_F a)
{
    __m128 factor = _mm_shuffle_ps(a, a, 0b11111111);
    __m128 rslt = _mm_mask_mul_ps(_mm_set_ps1(1.0f), 0b00000111, factor, a);
    return rslt;
}

