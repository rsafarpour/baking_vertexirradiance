#include "DrawSystem.h"

//#define GRAPHICAL_DEBUG

#define SLOT_PROJECTIONMATRIX 0

IDRedirect::IDRedirect()
{
    this->size_free = MAX_IDS;
    for(U32 i = 0; i < MAX_IDS; ++i)
    { this->free[MAX_IDS - i - 1] = i; }
}

Ref16 IDRedirect::insert(void* addr)
{
    if (this->size_free > 0)
    {
        U32 id = this->free[--this->size_free];
        this->addr[id] = addr;
        return id;
    }

    return REF16_INVALID;
}

void IDRedirect::remove(U32 id)
{
    if (id < MAX_IDS && this->addr[id] != nullptr)
    {
        this->addr[id] = nullptr;
        this->free[this->size_free] = id;
        ++this->size_free;
    }
}

void* IDRedirect::translate(U32 id)
{
    if (id < MAX_IDS)
    { return this->addr[id]; }

    return nullptr;
}

U16 IDRedirect::in_use()
{
    return (MAX_IDS - this->size_free);
}

struct Drawing
{
    IDXGIFactory*        dxgi;
    IDXGIAdapter*        adapt;
    IDXGISwapChain*      sc;
    ID3D11Device*        dev;
    ID3D11DeviceContext* ctx;
    D3D_FEATURE_LEVEL    level;
} drawing;

DrawSystem::DrawSystem(HeapMemory* mem_backend)
  : mem_meshes(mem_backend, sizeof(Mesh), MAX_IDS),
    mem_gpuprograms(mem_backend, sizeof(GPUProgram), MAX_IDS)
{}

DrawSystem::~DrawSystem()
{
    U32 id = 0;
    while (this->ids_meshes.in_use() > 0)
    {
        Mesh* m = (Mesh*)this->ids_meshes.translate(id);
        if (m->vbuffer != nullptr) { m->vbuffer->Release(); };
        if (m->ibuffer != nullptr) { m->ibuffer->Release(); };
        this->ids_meshes.remove(id++);
    }

    id = 0;
    while (this->ids_gpuprograms.in_use() > 0)
    {
        GPUProgram* p = (GPUProgram*)this->ids_gpuprograms.translate(id);
        if (p->layout != nullptr) { p->layout->Release(); }
        if (p->vs != nullptr)     { p->vs->Release(); }
        if (p->ps != nullptr)     { p->ps->Release(); }
        this->ids_gpuprograms.remove(id++);
    }

    while (this->ids_dbuffers.in_use() > 0)
    {
        ID3D11Buffer* b = (ID3D11Buffer*)this->ids_dbuffers.translate(id);
        if (b != nullptr) { b->Release(); }
        this->ids_dbuffers.remove(id++);
    }

    if (this->ds_state         != nullptr) { this->ds_state->Release();         }
    if (this->blend_state      != nullptr) { this->blend_state->Release();      }
    if (this->rasterizer_state != nullptr) { this->rasterizer_state->Release(); }
    if (this->ds_texture       != nullptr) { this->ds_texture->Release();       }
    if (this->dsv              != nullptr) { this->dsv->Release();              }
    if (this->rtv              != nullptr) { this->rtv->Release();              }

    drawing.dev->Release();
    drawing.ctx->Release();
    drawing.sc->Release();
    drawing.adapt->Release();
    drawing.dxgi->Release();
}

void DrawSystem::Setup(HWND hwnd)
{
    HRESULT hr;
    CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&drawing.dxgi);
    hr = drawing.dxgi->EnumAdapters(0, &drawing.adapt);

    D3D_FEATURE_LEVEL levels = {D3D_FEATURE_LEVEL_11_0};
    DXGI_SWAP_CHAIN_DESC sc_desc = {};
    sc_desc.BufferCount                        = 2;
    sc_desc.BufferDesc.Format                  = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
    sc_desc.BufferDesc.Height                  = 600;
    sc_desc.BufferDesc.RefreshRate.Denominator = 60;
    sc_desc.BufferDesc.RefreshRate.Numerator   = 1;
    sc_desc.BufferDesc.Scaling                 = DXGI_MODE_SCALING_UNSPECIFIED;
    sc_desc.BufferDesc.ScanlineOrdering        = DXGI_MODE_SCANLINE_ORDER_PROGRESSIVE;
    sc_desc.BufferDesc.Width                   = 800;
    sc_desc.BufferUsage                        = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sc_desc.Flags                              = 0;
    sc_desc.OutputWindow                       = hwnd;
    sc_desc.SampleDesc.Count                   = 1;
    sc_desc.SampleDesc.Quality                 = 0;
    sc_desc.SwapEffect                         = DXGI_SWAP_EFFECT_DISCARD;
    sc_desc.Windowed                           = true;

    hr = D3D11CreateDeviceAndSwapChain(
        drawing.adapt,
        D3D_DRIVER_TYPE_UNKNOWN,
        nullptr,
#ifdef GRAPHICAL_DEBUG
        D3D11_CREATE_DEVICE_DEBUG,
#else
        0, 
#endif
        &levels,
        1,
        D3D11_SDK_VERSION,
        &sc_desc,
        &drawing.sc,
        &drawing.dev,
        &drawing.level,
        &drawing.ctx);   

    // FIXED depth/stencil
    D3D11_DEPTH_STENCIL_DESC dsstate_desc = {};
    dsstate_desc.DepthEnable             = true;
    dsstate_desc.StencilEnable           = false;
    dsstate_desc.DepthFunc               = D3D11_COMPARISON_LESS;
    dsstate_desc.DepthWriteMask          = D3D11_DEPTH_WRITE_MASK_ALL;
    dsstate_desc.BackFace.StencilFailOp  = D3D11_STENCIL_OP_KEEP;
    dsstate_desc.BackFace.StencilPassOp  = D3D11_STENCIL_OP_KEEP;
    dsstate_desc.BackFace.StencilFunc    = D3D11_COMPARISON_ALWAYS;
    dsstate_desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    dsstate_desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    dsstate_desc.FrontFace.StencilFunc   = D3D11_COMPARISON_ALWAYS;
    drawing.dev->CreateDepthStencilState(&dsstate_desc, &this->ds_state);
    drawing.ctx->OMSetDepthStencilState(this->ds_state, 0);
    
    D3D11_TEXTURE2D_DESC dstexture_desc;
    dstexture_desc.ArraySize = 1;
    dstexture_desc.BindFlags = D3D10_BIND_DEPTH_STENCIL;
    dstexture_desc.CPUAccessFlags = 0;
    dstexture_desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    dstexture_desc.Height = 600;
    dstexture_desc.MipLevels = 1;
    dstexture_desc.MiscFlags = 0;
    dstexture_desc.SampleDesc.Count = 1;
    dstexture_desc.SampleDesc.Quality = 0;
    dstexture_desc.Usage = D3D11_USAGE_DEFAULT;
    dstexture_desc.Width = 800;
    hr = drawing.dev->CreateTexture2D(&dstexture_desc, nullptr, &this->ds_texture);
    
    D3D11_DEPTH_STENCIL_VIEW_DESC dsv_desc;
    dsv_desc.Flags = 0;
    dsv_desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    dsv_desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    dsv_desc.Texture2D.MipSlice = 0;
    
    drawing.dev->CreateDepthStencilView(this->ds_texture, &dsv_desc, &this->dsv);
    
    // FIXED blend & rasterizer state
    D3D11_BLEND_DESC blend_desc = {};
    blend_desc.AlphaToCoverageEnable = false;
    blend_desc.IndependentBlendEnable = false;
    blend_desc.RenderTarget[0].BlendEnable = true;
    blend_desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
    blend_desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
    blend_desc.RenderTarget[0].DestBlend = D3D11_BLEND_ZERO;
    blend_desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
    blend_desc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
    blend_desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
    blend_desc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
    
    D3D11_RASTERIZER_DESC rasterizer_desc = {};
    rasterizer_desc.AntialiasedLineEnable = false;
    rasterizer_desc.CullMode              = D3D11_CULL_NONE;
    rasterizer_desc.DepthBias             = 0;
    rasterizer_desc.DepthBiasClamp        = 0.0f;
    rasterizer_desc.DepthClipEnable       = true;
    rasterizer_desc.FillMode              = D3D11_FILL_SOLID;
    rasterizer_desc.FrontCounterClockwise = true;
    rasterizer_desc.MultisampleEnable     = false;
    rasterizer_desc.ScissorEnable         = false;
    rasterizer_desc.SlopeScaledDepthBias  = 0.0f;
    
    drawing.dev->CreateRasterizerState(&rasterizer_desc, &this->rasterizer_state);
    drawing.dev->CreateBlendState(&blend_desc, &this->blend_state);
    drawing.ctx->OMSetBlendState(this->blend_state, nullptr, 0xFFFFFFFF);
    drawing.ctx->RSSetState(this->rasterizer_state);
    
    // FIXED viewport
    D3D11_VIEWPORT vp = {};
    vp.TopLeftX = 0.0f;
    vp.TopLeftY = 0.0f;
    vp.Height = 600.0f;
    vp.Width = 800.0f;
    vp.MinDepth = 0.0f;
    vp.MaxDepth = 1.0f;
    
    drawing.ctx->RSSetViewports(1, &vp);
}

void DrawSystem::Present()
{
    drawing.sc->Present(1, 0);
}

void DrawSystem::Clear(float* color)
{
    ID3D11Texture2D* backbuffer;
    D3D11_RENDER_TARGET_VIEW_DESC rt;
    rt.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
    rt.Format = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
    rt.Texture2D.MipSlice = 0;

    if(this->rtv != nullptr)
    { this->rtv->Release(); }

    HRESULT hr;
    hr = drawing.sc->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&backbuffer);
    hr = drawing.dev->CreateRenderTargetView(backbuffer, nullptr, &this->rtv);
    backbuffer->Release();

    drawing.ctx->ClearRenderTargetView(rtv, color);
    drawing.ctx->ClearDepthStencilView(this->dsv, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0, 0);
    drawing.ctx->OMSetRenderTargets(1, &this->rtv, this->dsv);
}

Ref16 DrawSystem::UploadGPUProgram(const D3D11_INPUT_ELEMENT_DESC* layout, U32 layout_elements, GPUProgramData* program_data)
{
    GPUProgram* program = (GPUProgram*)this->mem_gpuprograms.Alloc();
    if (program != nullptr)
    {
        HRESULT hr;
        hr = drawing.dev->CreateVertexShader(program_data->vs_bytecode.ptr, program_data->vs_bytecode.bytes, nullptr, &program->vs);
        hr = drawing.dev->CreatePixelShader(program_data->ps_bytecode.ptr, program_data->ps_bytecode.bytes, nullptr, &program->ps);

        if (FAILED(drawing.dev->CreateInputLayout(layout, layout_elements,
            program_data->vs_bytecode.ptr, program_data->vs_bytecode.bytes, &program->layout)))
        {
            if (program->vs != nullptr) { program->vs->Release(); }
            if (program->ps != nullptr) { program->ps->Release(); }
            this->mem_gpuprograms.Free(program);
            return REF16_INVALID;
        }

        return this->ids_gpuprograms.insert(program);
    }

    return REF16_INVALID;
}

void DrawSystem::DrawTriangles(Ref16 ref_mesh)
{
    Mesh* mesh = (Mesh*)this->ids_meshes.translate(ref_mesh);
    if (mesh != nullptr)
    {
        
        UINT strides = mesh->vertex_stride;
        UINT offsets = 0;
        ID3D11Buffer* vbuffers = { mesh->vbuffer };
        ID3D11Buffer* ibuffers = { mesh->ibuffer };
        drawing.ctx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
        drawing.ctx->IASetVertexBuffers(0, 1, &vbuffers, &strides, &offsets);
        drawing.ctx->IASetIndexBuffer(mesh->ibuffer, DXGI_FORMAT_R32_UINT, 0);
    
        drawing.ctx->DrawIndexed(mesh->num_indices , 0, 0);
    }
}

void DrawSystem::DrawLines(Ref16 ref_mesh)
{
    Mesh* mesh = (Mesh*)this->ids_meshes.translate(ref_mesh);
    if (mesh != nullptr)
    {

        UINT strides = mesh->vertex_stride;
        UINT offsets = 0;
        ID3D11Buffer* vbuffers = {mesh->vbuffer};
        ID3D11Buffer* ibuffers = {mesh->ibuffer};
        drawing.ctx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
        drawing.ctx->IASetVertexBuffers(0, 1, &vbuffers, &strides, &offsets);
        drawing.ctx->IASetIndexBuffer(mesh->ibuffer, DXGI_FORMAT_R32_UINT, 0);

        drawing.ctx->DrawIndexed(mesh->num_indices, 0, 0);
    }
}

void DrawSystem::SetGPUProgram(Ref16 ref_program)
{
    GPUProgram* gpu_program = (GPUProgram*)this->ids_gpuprograms.translate(ref_program);
    if (gpu_program != nullptr)
    {
        drawing.ctx->IASetInputLayout(gpu_program->layout);
        drawing.ctx->VSSetShader(gpu_program->vs, nullptr, 0);
        drawing.ctx->PSSetShader(gpu_program->ps, nullptr, 0);
    }
}

void DrawSystem::SetProjectionMatrix(Ref16 ref_buffer)
{
    ID3D11Buffer* dbuffer = (ID3D11Buffer*)this->ids_dbuffers.translate(ref_buffer);
    if (dbuffer != nullptr)
    {
        drawing.ctx->VSSetConstantBuffers(SLOT_PROJECTIONMATRIX, 1, &dbuffer);
    }
}

void DrawSystem::UpdateDynamicBuffer(Ref16 ref_buffer, BufferData* data)
{

    ID3D11Buffer* dbuffer = (ID3D11Buffer*)this->ids_dbuffers.translate(ref_buffer);
    if (dbuffer != nullptr)
    {
        D3D11_MAPPED_SUBRESOURCE mapped = {};

        drawing.ctx->Map(dbuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped);
        memcpy(mapped.pData, data->data, data->bytes);
        drawing.ctx->Unmap(dbuffer, 0);
    }
}

Ref16 DrawSystem::UploadDynamicBuffer(BufferData* data)
{
    D3D11_BUFFER_DESC desc;
    desc.BindFlags           = D3D11_BIND_CONSTANT_BUFFER;
    desc.ByteWidth           = (data->bytes + 15) & ~15; // Make multiple of 16
    desc.CPUAccessFlags      = D3D11_CPU_ACCESS_WRITE;
    desc.MiscFlags           = 0;
    desc.StructureByteStride = 0;
    desc.Usage               = D3D11_USAGE_DYNAMIC;

    D3D11_SUBRESOURCE_DATA subresource_data = {};

    if (data->data != nullptr)
    { subresource_data.pSysMem = data->data; }

    ID3D11Buffer* buffer;
    if (SUCCEEDED(drawing.dev->CreateBuffer(&desc, &subresource_data, &buffer)))
    {
        return this->ids_dbuffers.insert(buffer);
    }

    return REF16_INVALID;
}

Ref16 DrawSystem::UploadMesh(U32 vertex_stride, MeshData* mesh)
{
    D3D11_BUFFER_DESC vbuffer_desc = {};
    vbuffer_desc.BindFlags           = D3D11_BIND_VERTEX_BUFFER;
    vbuffer_desc.ByteWidth           = vertex_stride * mesh->num_vertices;
    vbuffer_desc.CPUAccessFlags      = 0;
    vbuffer_desc.MiscFlags           = 0;
    vbuffer_desc.StructureByteStride = 0;
    vbuffer_desc.Usage               = D3D11_USAGE_DEFAULT;

    D3D11_BUFFER_DESC ibuffer_desc = {};
    ibuffer_desc.BindFlags           = D3D11_BIND_INDEX_BUFFER;
    ibuffer_desc.ByteWidth           = sizeof(U32) * mesh->num_indices;
    ibuffer_desc.CPUAccessFlags      = 0;
    ibuffer_desc.MiscFlags           = 0;
    ibuffer_desc.StructureByteStride = 0;
    ibuffer_desc.Usage               = D3D11_USAGE_DEFAULT;

    Mesh* m = (Mesh*) this->mem_meshes.Alloc();
    if (m != nullptr)
    {
        HRESULT hr_vert;
        D3D11_SUBRESOURCE_DATA vdata = {};
        vdata.pSysMem = mesh->vertices;
        hr_vert = drawing.dev->CreateBuffer(&vbuffer_desc, &vdata, &m->vbuffer);

        HRESULT hr_ind;
        D3D11_SUBRESOURCE_DATA idata = {};
        idata.pSysMem = mesh->indices;
        hr_ind = drawing.dev->CreateBuffer(&ibuffer_desc, &idata, &m->ibuffer);

        if (SUCCEEDED(hr_vert) && SUCCEEDED(hr_ind))
        {
            m->num_indices = mesh->num_indices;
            m->vertex_stride = vertex_stride;

            return this->ids_meshes.insert(m);
        }

        this->mem_meshes.Free(m);
    }

    return REF16_INVALID;
}

void DrawSystem::FreeMesh(Ref16 ref_mesh)
{
    Mesh* mesh = (Mesh*)this->ids_meshes.translate(ref_mesh);
    if (mesh != nullptr)
    {
        mesh->ibuffer->Release();
        mesh->vbuffer->Release();
        this->ids_meshes.remove(ref_mesh);
    }
}

void DrawSystem::FreeGPUProgram(Ref16 ref_program)
{
    GPUProgram* gpu_program = (GPUProgram*)this->ids_gpuprograms.translate(ref_program);
    if (gpu_program != nullptr)
    {
        gpu_program->vs->Release();
        gpu_program->ps->Release();
        gpu_program->layout->Release();
        this->ids_gpuprograms.remove(ref_program);
    }
}

void DrawSystem::FreeDynamicBuffer(Ref16 ref_buffer)
{
    ID3D11Buffer* dbuffer = (ID3D11Buffer*)this->ids_dbuffers.translate(ref_buffer);
    if (dbuffer != nullptr)
    {
        dbuffer->Release();
        this->ids_dbuffers.remove(ref_buffer);
    }
}