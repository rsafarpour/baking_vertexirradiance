#pragma once

#include <cassert>
#include "types.h"

struct handle;

struct block_header_t
{
    U8 order;
    U8 pad[3];
};

struct block_t
{
    block_t* prev;
    block_t* next;
};

struct order_t
{
    block_t* free;
    BYTE*    pair_state;
    U32      free_blocks;
};

class HeapMemory
{
public:
    HeapMemory(U8 max_order);
    ~HeapMemory();

    void* Alloc(U64 bytes, U8 align = 1);
    void  Free(void* addr);

protected:
    void*    base;
    U8       max_order;
    order_t* order_list;
};