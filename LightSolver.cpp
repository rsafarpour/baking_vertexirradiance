#include "LightSolver.h"
#include "MonteCarlo.h"
#include "Sampling.h"
#include "constants.h"
#define NUM_RAYS 128

LightSolver::LightSolver(HeapMemory* mem_backend)
   : memory(mem_backend, 1024*4)
{}

void cos_weight(Vector3 normal, Vector3* dirs, Vector3* radiance, U32 num_dirs)
{
    for (U32 i = 0; i < num_dirs; ++i)
    {
        float cos_theta = dot(dirs[i], normal);
        radiance[i] = scale(cos_theta, radiance[i]);
    }
}

void lambert_brdf(Vector3* radiance, U32 num_dirs)
{
    for (U32 i = 0; i < num_dirs; ++i)
    {
        radiance[i] = scale(1/Trigonometry::Pi, radiance[i]);
    }
}

void solve_emittance(StaticGeometry* geom, ColorRGBA_F* irradiance)
{
    U32 num_verts = geom->GetNumVertices();
    for (U32 i = 0; i < num_verts; ++i)
    {
        Vector3 position;
        Vector3 normal;
        geom->GetPositions(&i, &position, 1);
        geom->GetNormals(&i, &normal, 1);

        Vector3 dirs[NUM_RAYS];
        HemisphericalSamples(normal, NUM_RAYS, dirs);
        
        U32 prims[NUM_RAYS];
        Vector3 barycentric_coords[NUM_RAYS];
        geom->Raycast(position, dirs, NUM_RAYS, prims, barycentric_coords);
        
        Vector3 radiance[NUM_RAYS];
        geom->InterpolateEmittance(prims, barycentric_coords, NUM_RAYS, radiance);
        
        cos_weight(normal, dirs, radiance, NUM_RAYS);
        irradiance[i] = MonteCarlo::Spherical::integrate(radiance, NUM_RAYS);
    }
}

void solve_reflection(StaticGeometry* geom, ColorRGBA_F* last_bounce, ColorRGBA_F* irradiance)
{
    U32 num_verts = geom->GetNumVertices();
    for (U32 i = 0; i < num_verts; ++i)
    {
        Vector3 position;
        Vector3 normal;
        geom->GetPositions(&i, &position, 1);
        geom->GetNormals(&i, &normal, 1);

        Vector3 dirs[NUM_RAYS];
        HemisphericalSamples(normal, NUM_RAYS, dirs);

        U32 prims[NUM_RAYS];
        Vector3 barycentric_coords[NUM_RAYS];
        geom->Raycast(position, dirs, NUM_RAYS, prims, barycentric_coords);

        Vector3 radiance[NUM_RAYS];
        geom->InterpolateAlbedo(prims, barycentric_coords, last_bounce, NUM_RAYS, radiance);

        lambert_brdf(radiance, NUM_RAYS);
        cos_weight(normal, dirs, radiance, NUM_RAYS);
        last_bounce[i] = MonteCarlo::Spherical::integrate(radiance, NUM_RAYS);

        irradiance[i] = irradiance[i] + last_bounce[i];
    }
}

void LightSolver::SolveGeometryRadiance(StaticGeometry* geom, U32 bounces, ColorRGBA_F* irradiance)
{
    U32 num_verts = geom->GetNumVertices();

    solve_emittance(geom, irradiance);
    
    // Initialize last bounce values with direct irradiance
    ColorRGBA_F* last_bounce = (ColorRGBA_F*)memory.Alloc(sizeof(ColorRGBA_F) * num_verts, sizeof(ColorRGBA_F));
    for (U32 i = 0; i < num_verts; ++i)
    { last_bounce[i] = irradiance[i]; }
    
    for (U32 i = 0; i < bounces; ++i)
    {
        solve_reflection(geom, last_bounce, irradiance);
    }

    this->memory.Clear();
}
