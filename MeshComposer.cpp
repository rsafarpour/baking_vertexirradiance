#include "MeshComposer.h"
#include "VertexLayouts.h"

#define VERTEX_FLOATS 15

void compose_xyz(Vector3* input, U32 offset, U32 stride, U32 num, float* out)
{
    for (U32 i = 0; i < num; ++i)
    {
        out[i*stride + offset + 0] = getx(input[i]);
        out[i*stride + offset + 1] = gety(input[i]);
        out[i*stride + offset + 2] = getz(input[i]);
    }
}

void compose_rgb(ColorRGBA_F* input, U32 offset, U32 stride, U32 num, float* out)
{
    for (U32 i = 0; i < num; ++i)
    {
        out[i*stride + offset + 0] = getx(input[i]);
        out[i*stride + offset + 1] = gety(input[i]);
        out[i*stride + offset + 2] = getz(input[i]);
    }
}

MeshComposer::MeshComposer(HeapMemory* mem_backend)
  : memory(mem_backend, 16*1024)
{}

Ref16 MeshComposer::UploadComposed(DrawSystem* drawing, StaticGeometry* geometry, ColorRGBA_F* irradiance)
{
    U32 num_vertices = geometry->GetNumVertices();
    U32* linear_indices = (U32*)memory.Alloc(sizeof(U32)*num_vertices, sizeof(U32));
    for (U32 i = 0; i < num_vertices; ++i) { linear_indices[i] = i; }

    // Vertex contains 3x position, 3x normal, 3x albedo, 3x irradiance 3x emittance 
    float* vertices = (float*) this->memory.Alloc(sizeof(float)*VERTEX_FLOATS*num_vertices, sizeof(float));

    Vector3* positions = (Vector3*)this->memory.Alloc(sizeof(Vector3)*num_vertices, sizeof(Vector3));
    geometry->GetPositions(linear_indices, positions, num_vertices);
    compose_xyz(positions, 0, VERTEX_FLOATS, num_vertices, vertices);
    this->memory.SetTop(positions);

    Vector3* normals = (Vector3*)this->memory.Alloc(sizeof(Vector3)*num_vertices, sizeof(Vector3));
    geometry->GetNormals(linear_indices, normals, num_vertices);
    compose_xyz(normals, 3, VERTEX_FLOATS, num_vertices, vertices);
    this->memory.SetTop(normals);

    ColorRGBA_F* albedo = (ColorRGBA_F*)this->memory.Alloc(sizeof(ColorRGBA_F)*num_vertices, sizeof(ColorRGBA_F));
    geometry->GetAlbedo(linear_indices, albedo, num_vertices);
    compose_rgb(albedo, 6, VERTEX_FLOATS, num_vertices, vertices);
    this->memory.SetTop(albedo);

    compose_rgb(irradiance, 9, VERTEX_FLOATS, num_vertices, vertices);

    ColorRGBA_F* emittance = (ColorRGBA_F*)this->memory.Alloc(sizeof(ColorRGBA_F)*num_vertices, sizeof(ColorRGBA_F));
    geometry->GetEmittance(linear_indices, emittance, num_vertices);
    compose_rgb(emittance, 12, VERTEX_FLOATS, num_vertices, vertices);
    this->memory.SetTop(emittance);




    U32 num_indices = geometry->GetNumIndices();
    U32* indices = (U32*)this->memory.Alloc(sizeof(U32)*VERTEX_FLOATS*num_indices, sizeof(U32));
    geometry->GetIndices(indices, num_indices);

    MeshData data = {};
    data.vertices = vertices;
    data.indices = indices;
    data.num_indices = num_indices;
    data.num_vertices = num_vertices;

    Ref16 ref = drawing->UploadMesh(StaticMeshes::VertexStride, &data);
    this->memory.Clear();

    return ref;
}