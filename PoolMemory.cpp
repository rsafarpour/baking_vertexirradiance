#include "PoolMemory.h"
#include <assert.h>
#include <memory>

PoolMemory::PoolMemory(HeapMemory* memory, U32 object_size, U32 num_objects)
  : memory(memory)
{
    // Each object must be able to store a pointer
    // to the next free element
    assert(object_size >= sizeof(void*));

    this->object_size = object_size;
    this->base = memory->Alloc(object_size * num_objects);
    this->head = 0;
    this->num_free = num_objects;
    this->num_init = 0;

    assert(this->base != nullptr);
}

PoolMemory::~PoolMemory()
{
    memory->Free(this->base);
}

void* PoolMemory::Alloc()
{
    if (this->num_free > 0)
    {
        IADR blk_addr = (IADR)this->base + (IADR)(this->head * object_size);
        if (this->num_init != this->head)
        {
            this->head = ((U32*)blk_addr)[0];
        }
        else
        {
            this->num_init += 1;
            this->head = this->num_init;
        }

        this->num_free -= 1;
        return (void*) blk_addr;
    }

    return nullptr;
}

void PoolMemory::Free(void* ptr)
{
    // Reject invalid free calls
    IADR offset = (IADR)ptr - (IADR)this->base;
    if (offset % this->object_size == 0)
    {
        U32 blk_no = offset / (IADR)this->object_size;
        ((U32*)ptr)[0] = this->head;
        this->head = blk_no;

        this->num_free += 1;
    }

    
}