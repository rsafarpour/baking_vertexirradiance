cbuffer ProjMatrix : register(b0)
{
	float4x4 _proj;
}

struct VertexIn
{
    float3 position   : POSITION;
};

struct VertexOut
{
    float4 position : SV_POSITION;
};

VertexOut main(VertexIn i, uint id :SV_VertexID)
{
	float4 position = float4(i.position, 1.0f);

    VertexOut o;
    o.position = mul(_proj, position);
	return o;
}