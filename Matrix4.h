#pragma once

#include <algorithm>
#include <xmmintrin.h>

// Row major 4x4 matrix
struct Matrix4
{
    __m128 _0;
    __m128 _1;
    __m128 _2;
    __m128 _3;
};

inline void transpose(Matrix4* m)
{
    _MM_TRANSPOSE4_PS(m->_0, m->_1, m->_2, m->_3);
}

inline void set(Matrix4* m, unsigned i, unsigned j, float value)
{
    if (i < 4 && j < 4)
    {
        float* row = nullptr;
        if (i == 0) { row = (float*)&m->_0; }
        if (i == 1) { row = (float*)&m->_1; }
        if (i == 2) { row = (float*)&m->_2; }
        if (i == 3) { row = (float*)&m->_3; }
        row[j] = value;
    }
}

inline void setrow(Matrix4* m, unsigned i, Vector3 values)
{
    if (i == 0) { m->_0 = values; }
    if (i == 1) { m->_1 = values; }
    if (i == 2) { m->_2 = values; }
    if (i == 3) { m->_3 = values; }
}

inline Matrix4 mul(const Matrix4* m1, Matrix4 m2)
{
    transpose(&m2);
    Matrix4 result;
    __m128 tmp1 = _mm_mul_ps(m1->_0, m2._0);
    __m128 tmp2 = _mm_mul_ps(m1->_0, m2._1);
    __m128 tmp3 = _mm_mul_ps(m1->_0, m2._2);
    __m128 tmp4 = _mm_mul_ps(m1->_0, m2._3);

    __asm {
        movaps xmm0, tmp1;
        movaps xmm1, tmp2;
        movaps xmm2, tmp3;
        movaps xmm3, tmp4;
        haddps xmm0, xmm1;
        haddps xmm0, xmm0;
        haddps xmm2, xmm3;
        haddps xmm2, xmm2;
        shufps xmm0, xmm2, 0x44; 
        movaps result._0, xmm0;
    }

    tmp1 = _mm_mul_ps(m1->_1, m2._0);
    tmp2 = _mm_mul_ps(m1->_1, m2._1);
    tmp3 = _mm_mul_ps(m1->_1, m2._2);
    tmp4 = _mm_mul_ps(m1->_1, m2._3);

    __asm {
        movaps xmm0, tmp1;
        movaps xmm1, tmp2;
        movaps xmm2, tmp3;
        movaps xmm3, tmp4;
        haddps xmm0, xmm1;
        haddps xmm0, xmm0;
        haddps xmm2, xmm3;
        haddps xmm2, xmm2;
        shufps xmm0, xmm2, 0x44;
        movaps result._1, xmm0;
    }

    tmp1 = _mm_mul_ps(m1->_2, m2._0);
    tmp2 = _mm_mul_ps(m1->_2, m2._1);
    tmp3 = _mm_mul_ps(m1->_2, m2._2);
    tmp4 = _mm_mul_ps(m1->_2, m2._3);

    __asm {
        movaps xmm0, tmp1;
        movaps xmm1, tmp2;
        movaps xmm2, tmp3;
        movaps xmm3, tmp4;
        haddps xmm0, xmm1;
        haddps xmm0, xmm0;
        haddps xmm2, xmm3;
        haddps xmm2, xmm2;
        shufps xmm0, xmm2, 0x44;
        movaps result._2, xmm0;
    }

    tmp1 = _mm_mul_ps(m1->_3, m2._0);
    tmp2 = _mm_mul_ps(m1->_3, m2._1);
    tmp3 = _mm_mul_ps(m1->_3, m2._2);
    tmp4 = _mm_mul_ps(m1->_3, m2._3);

    __asm {
        movaps xmm0, tmp1;
        movaps xmm1, tmp2;
        movaps xmm2, tmp3;
        movaps xmm3, tmp4;
        haddps xmm0, xmm1;
        haddps xmm0, xmm0;
        haddps xmm2, xmm3;
        haddps xmm2, xmm2;
        shufps xmm0, xmm2, 0x44;
        movaps result._3, xmm0;
    }

    return result;
}

inline Matrix4 PerspectiveProjectionMatrix(float fov_y, float aspect_ratio, float n, float f)
{
    return {
        1/(tan(fov_y/2)), 0.0,           0.0,                                  0.0,
        0.0,                             aspect_ratio * 1/(tan(fov_y/2)),      0.0,               0.0,
        0.0,                             0.0,              -(n + f)/(f - n),  -(2*n*f)/(f-n),
        0.0,                             0.0,              -1.0,               0.0
    };
}