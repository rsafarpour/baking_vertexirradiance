#pragma once

#include "Ray.h"
#include "HeapMemory.h"
#include "StackMemory.h"
#include "PoolMemory.h"
#include "Color.h"
#include "sBVH.h"

struct VertexAttrs
{
    Vector3     position;
    Vector3     normal;
    ColorRGBA_F emittance;
    ColorRGBA_F albedo;
};

class StaticGeometry
{
public:
    StaticGeometry(HeapMemory* mem_backend, sBVH* bvh);

    U32  GetNumIndices();
    U32  GetNumVertices();

    void InsertPrimitives(Vector3* pos, Vector3* normal, ColorRGBA_F* albedo, ColorRGBA_F* emittance, U32 num_verts, U32* indices, U32 num_indices);
    void Raycast(Vector3 position, Vector3* dirs, U32 num_rays, U32* prim_indices, Vector3* bary_coords);
    void InterpolateAlbedo(U32* prim_indices, Vector3* bary_coords, ColorRGBA_F* last_bounce, U32 num_points, ColorRGBA_F* colors);
    void InterpolateEmittance(U32* prim_indices, Vector3* bary_coords, U32 num_points, ColorRGBA_F* colors);
    void GetNormals(U32* vert_indices, Vector3* normals, U32 num);
    void GetPositions(U32* vert_indices, Vector3* positions, U32 num);
    void GetAlbedo(U32* vert_indices, Vector3* albedo, U32 num);
    void GetEmittance(U32* vert_indices, Vector3* emittance, U32 num);
    void GetIndices(U32* indices, U32 num);

private:
    HeapMemory*  mem_backend;
    sBVH*        bvh;
    StackMemory  vertex_memory;
    StackMemory  index_memory;
    VertexAttrs* vertices;
    U32*         indices;
    U32          num_verts;
    U32          num_inds;
};
