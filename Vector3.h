#pragma once

#include <cmath>
#include <xmmintrin.h>
#include <immintrin.h>

#define EPSILON_EQUALS 0.00001f

using Vector3 = __m128;

inline Vector3 min(const Vector3 a, const Vector3 b)
{
    return _mm_min_ps(b, a);
}

inline Vector3 max(const Vector3 a, const Vector3 b)
{
    return _mm_max_ps(b, a);
}

inline Vector3 operator-(const Vector3 a, const Vector3 b)
{
    return _mm_sub_ps(a, b);
}

inline Vector3 operator+(const Vector3 a, const Vector3 b)
{
    return _mm_add_ps(a, b);
}

inline Vector3 operator<(const Vector3 a, const Vector3 b)
{
    return _mm_cmplt_ps(a, b);
}

inline Vector3 operator>(const Vector3 a, const Vector3 b)
{
    return _mm_cmpgt_ps(a, b);
}

inline Vector3 scale(const float s, const Vector3 b)
{
    __m128 scalar = _mm_load_ps1(&s);
    return _mm_mul_ps(scalar, b);
}

inline float dot(const Vector3 a, const Vector3 b)
{
    __m128 tmp = _mm_mul_ps(a, b);
    tmp = _mm_hadd_ps(tmp, tmp);
    tmp = _mm_hadd_ps(tmp, tmp);
    return _mm_cvtss_f32(tmp);
}

inline Vector3 cross(const Vector3 a, const Vector3 b)
{
    __m128 op0 = _mm_shuffle_ps(a, a, 0b11001001);
    __m128 op1 = _mm_shuffle_ps(b, b, 0b11010010);
    __m128 op2 = _mm_shuffle_ps(a, a, 0b11010010);
    __m128 op3 = _mm_shuffle_ps(b, b, 0b11001001);

    __m128 l = _mm_mul_ps(op0, op1);
    __m128 r = _mm_mul_ps(op2, op3);
    return _mm_sub_ps(l, r);
}

inline Vector3 orthogonal_of(const Vector3 a)
{
    // Find a vector that is definitely different 
    // from a (unless it is (0,0,0)
    __m128 tmp1 = _mm_shuffle_ps(a, a, 0b11100001);
    __m128 tmp2 = _mm_shuffle_ps(a, a, 0b11011000);
    __m128 tmp = _mm_add_ps(tmp1, tmp2);

    return cross(a, tmp);
}

inline bool equals(const Vector3 a, const Vector3 b)
{
    // Calculate absolute of difference
    __m128 negzero = _mm_set_ps1(-0.0);
    __m128 adiff = _mm_andnot_ps(negzero, _mm_sub_ps(a, b));

    // Acceptable difference epsilon
    __m128 eps = _mm_set_ps1(EPSILON_EQUALS);
    __m128 cmp = _mm_cmplt_ps(eps, adiff);
    int rslt = _mm_movemask_ps(cmp);

    return (rslt == 0);
}

inline float getx(const Vector3 a)
{ 
    return _mm_cvtss_f32(a);
}

inline float gety(const Vector3 a)
{
    float out[4];
    _mm_store_ps(out, a);

    return out[1];
}

inline float getz(const Vector3 a)
{
    float out[4];
    _mm_store_ps(out, a);

    return out[2];
}

inline void get_comps(const Vector3 a, float* out)
{
    _mm_store_ps(out, a);
}

inline float norm(const Vector3 a)
{
    return sqrt(dot(a, a));
}

inline Vector3 normalized(const Vector3 a)
{
    float len = norm(a);
    if (len > 0.0f)
    {
        __m128 factors = _mm_load_ps1(&len);
        return _mm_div_ps(a, factors);
    }

    return _mm_set_ps1(NAN);
}

inline bool operator==(const Vector3 a, const Vector3 b)
{
    __m128 cmp = _mm_cmpeq_ps(a, b);
    return (_mm_movemask_ps(cmp) == 0b00001111);
}

inline bool operator!=(const Vector3 a, const Vector3 b)
{
    return !(a == b);
}

inline Vector3 hadamard(const Vector3 a, const Vector3 b)
{
    return _mm_mul_ps(a, b);
}

inline Vector3 ihadamard(const Vector3 a, const Vector3 b)
{
    __m128 bb = b + Vector3{0.0f, 0.0f, 0.0f, 1.0f};
    return _mm_div_ps(a, bb);
}