#pragma once

#include "Vector3.h"

struct Ray
{
    Vector3 o; /* Ray origin    */
    Vector3 d; /* Ray direction */
};