#include "search.h"

U32 radix_upperbound(U32* keys, U32 bit, U32 range_low, U32 range_high)
{
    if ((keys[range_low] & (1 << bit)) == (keys[range_high - 1] & (1 << bit)))
    { return range_high; }

    while (range_high - range_low > 1)
    {
        U32 current_index = range_low + (range_high - range_low + 1) / 2U;

        if ((keys[current_index] & (1 << bit)) > 0)
        { range_high = current_index; }
        else
        {
            range_low = current_index;
        }
    }

    if ((keys[range_high] & (1 << bit)) == 0)
    {
        return range_high + 1;
    }

    return range_high;
}