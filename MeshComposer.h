#pragma once

#include "HeapMemory.h"
#include "StackMemory.h"
#include "StaticGeometry.h"
#include "DrawSystem.h"

class MeshComposer
{
public:
    MeshComposer(HeapMemory* mem_backend);

    Ref16 UploadComposed(DrawSystem* drawing, StaticGeometry* geometry, ColorRGBA_F* irradiance);

private:
    StackMemory memory;
};
