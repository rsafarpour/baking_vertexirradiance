#pragma once

#include "HeapMemory.h"

class StackMemory
{
public:
    StackMemory(HeapMemory* memory, U64 bytes);
    ~StackMemory();

    void* Alloc(U64 bytes, U8 align = 1);
    void* GetTop();
    void  SetTop(void* ptr);
    void  Clear();

private:
    U64         free;
    void*       top;
    void*       base;
    HeapMemory* memory;
};