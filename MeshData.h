#pragma once

#include "types.h"

struct MeshData
{
    float* vertices;
    U32*   indices;
    U32    num_vertices;
    U32    num_indices;
};
