#pragma once

#include "AABB.h"
#include "Ray.h"
#include "HeapMemory.h"
#include "StackMemory.h"

class sBVH
{
public:
    sBVH(HeapMemory* mem_backend);

    void InsertPrimitives(Vector3* vertices, U32* indices, U32 num_prims);
    void RaycastCandidates(Ray r, U32 max_num, U32* candidate_prims, U32* out_num);
    void GetVolume(U32 index, AABB* aabb);

protected:
    AABB        root_volume;
    StackMemory vol_memory;
    StackMemory work_memory;
    U32         num_prims;
    U32         num_volumes;
    AABB*       volumes;
    U32*        skip_vals;
    U32*        prim_ids;
    U32*        volume_base;
    U32*        volume_range;
};
