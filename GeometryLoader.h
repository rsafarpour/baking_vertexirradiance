#pragma once

#include <string>
#include "DrawSystem.h"
#include "MeshData.h"
#include "sBVH.h"
#include "StaticGeometry.h"
#include "StackMemory.h"
#include "types.h"

class GeometryLoader
{
public:
    GeometryLoader(HeapMemory* mem_backend);

    void Load(StaticGeometry* geometry, const char* filename);

private:
    StackMemory memory;
};


