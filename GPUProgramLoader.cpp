#define _CRT_SECURE_NO_DEPRECATE

#include "GPUProgramLoader.h"
#include <stdio.h>
#include <string>
#include <d3dcompiler.h>
#include "FileIO.h"

#pragma comment(lib, "D3DCompiler.lib")

#define SIZE_BUFFER 1024

GPUProgramData GPUProgramLoader::Load(StackMemory* stack, const char* vs_path,
    const char* ps_path)
{
    void* top = stack->GetTop();
    U64 vs_filesize = PokeFilesize(vs_path);
    U64 ps_filesize = PokeFilesize(vs_path);
    char* vs_content = (char*) stack->Alloc(vs_filesize);
    char* ps_content = (char*) stack->Alloc(ps_filesize);
    if (vs_content == 0 || ps_content == 0)
    { 
        stack->SetTop(top);
        return {}; 
    }

    U64 vs_codesize = ReadFile(vs_path, vs_content, vs_filesize);
    U64 ps_codesize = ReadFile(ps_path, ps_content, ps_filesize);
    if (vs_codesize == 0 || ps_codesize == 0)
    { 
        stack->SetTop(top);
        return {}; 
    }

    ID3DBlob* vs_blob;
    ID3DBlob* err_msg;
    if (FAILED(D3DCompile(
        vs_content,
        (SIZE_T)vs_codesize,
        vs_path,
        nullptr,
        nullptr,
        "main",
        "vs_5_0",
        D3DCOMPILE_DEBUG,
        0,
        &vs_blob,
        &err_msg)))
    {
        OutputDebugStringA((const char*) err_msg->GetBufferPointer());
        err_msg->Release();
        stack->SetTop(top);
        return {};
    }

    ID3DBlob* ps_blob;
    if (FAILED(D3DCompile(
        ps_content,
        (SIZE_T)ps_codesize,
        ps_path,
        nullptr,
        nullptr,
        "main",
        "ps_5_0",
        D3DCOMPILE_DEBUG,
        0,
        &ps_blob,
        &err_msg)))
    {
        OutputDebugStringA((const char*)err_msg->GetBufferPointer());
        err_msg->Release();
        stack->SetTop(top);
        return {};
    }

 
    void* vs_bytecode = stack->Alloc(vs_blob->GetBufferSize());
    void* ps_bytecode = stack->Alloc(ps_blob->GetBufferSize());

    memcpy(vs_bytecode, vs_blob->GetBufferPointer(), vs_blob->GetBufferSize());
    memcpy(ps_bytecode, ps_blob->GetBufferPointer(), ps_blob->GetBufferSize());

    GPUProgramData data = {};
    data.vs_bytecode.ptr = vs_bytecode;
    data.vs_bytecode.bytes = vs_blob->GetBufferSize();
    data.ps_bytecode.ptr = ps_bytecode;
    data.ps_bytecode.bytes = ps_blob->GetBufferSize();
    vs_blob->Release();
    ps_blob->Release();
    return data;
}