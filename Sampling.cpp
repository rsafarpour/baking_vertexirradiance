#include "Sampling.h"
#include "PolarCoordinates.h"
#include "Matrix3.h"
#include <random>

void HemisphericalSamples(Vector3 up, U32 num_dirs, Vector3* dirs)
{
    // Change of basis function for current vertex
    Vector3 right = orthogonal_of(up);
    Vector3 forward = cross(up, right);
    Matrix3 tnb = {right, up, forward};
    transpose(&tnb);

    constexpr float pi = 3.14159265359f;
    std::random_device rd;
    std::uniform_real_distribution<float> dist_r(0, 1.0);
    std::uniform_real_distribution<float> dist_theta(0, 2 * pi);

    for (U32 i = 0; i < num_dirs; i++)
    {
        float r = sqrt(dist_r(rd));
        float theta = 2 * pi*dist_theta(rd);

        Vector3 cartesian = PolarCoordinates::ToCartesian({r, theta});
        float x = getx(cartesian);
        float z = gety(cartesian);
        float y = sqrt(1 - x * x - z * z);

        Vector3 ts_dir = normalized({x, y, z}); // tangent space
        Vector3 ws_dir = mul(tnb, ts_dir);      // world space

        dirs[i] = ws_dir;
    }
}