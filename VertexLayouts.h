#pragma once

#include "types.h"
#include <d3d11.h>

namespace StaticMeshes
{
    const D3D11_INPUT_ELEMENT_DESC VertexLayout[] = {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "ALBEDO", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "IRRADIANCE", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 36, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "EMITTANCE", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 48, D3D11_INPUT_PER_VERTEX_DATA, 0 }
    };

    const U32 VertexElements = 5;
    const U32 VertexStride = sizeof(float) * 15;
}

namespace DebugRays
{
    const D3D11_INPUT_ELEMENT_DESC VertexLayout[] = {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    };

    const U32 VertexElements = 1;
    const U32 VertexStride = sizeof(float) * 3;
}

namespace DebugBox
{
    const D3D11_INPUT_ELEMENT_DESC VertexLayout[] = {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    };

    const U32 VertexElements = 1;
    const U32 VertexStride = sizeof(float) * 3;
}
