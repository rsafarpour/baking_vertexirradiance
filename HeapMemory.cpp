#include "HeapMemory.h"
#include <memory>

#define POW2(exponent) (1 << (exponent))
#define ORDER_BLOCKBYTES(order) (POW2(order)*1024)

#define MIN_ORDER 2
#define BYTE_BITS 8

U32 bit_id(void* base, block_t* block, U8 order)
{
    U32 block_id = ((IADR)block - (IADR)base) / ORDER_BLOCKBYTES(order);
    return block_id >> 1; // == div 2
}

void toggle_state(order_t* order_entry, U32 bit)
{
    U8 byte = bit / BYTE_BITS;
    bit = bit % BYTE_BITS;

    order_entry->pair_state[byte] ^= (1U << bit);
}

void push_free_block(void* base, order_t* order_entry, block_t* block, U8 order)
{
    if (order_entry->free != nullptr)
    {
        order_entry->free->prev = block;
    }

    block->prev = nullptr;
    block->next = order_entry->free;
    order_entry->free = block; 
    order_entry->free_blocks += 1;

    toggle_state(order_entry, bit_id(base, block, order));
}

block_t* pop_free_block(void* base, order_t* order_entry, U8 order)
{
    if (order_entry->free != nullptr)
    {
        block_t* popped = order_entry->free;
        order_entry->free = order_entry->free->next;
        order_entry->free_blocks -= 1;

        if (order_entry->free != nullptr)
        { order_entry->free->prev = nullptr; }

        toggle_state(order_entry, bit_id(base, popped, order));
        return popped;
    }

    return nullptr;
}

void to_listfront(order_t* order_entry, block_t* block)
{
    if (order_entry->free != block)
    {
        if (block->prev != nullptr) { block->prev->next = block->next; }
        if (block->next != nullptr) { block->next->prev = block->prev; }

        if (order_entry->free != nullptr)
        { 
            order_entry->free->prev = block;
        }

        block->next = order_entry->free;
        block->prev = nullptr;
        order_entry->free = block;
    }
}

bool is_state_set(order_t* order_entry, U32 bit)
{
    U8 byte = bit / BYTE_BITS;
    bit = bit % BYTE_BITS;

    return (order_entry->pair_state[byte] & (1U << bit));
}

void ensure_order_block(void* base, order_t* order_list, U8 max_order, U8 order)
{
    U8 current_order = order;
    while (order_list[current_order].free == nullptr)
    { 
        if (current_order == max_order)
        { return; }

        ++current_order; 
    }
    
    while (current_order != order)
    {
        block_t* block = pop_free_block(base, &order_list[current_order], current_order);
    
        U8 prev_order = current_order - 1;
        block_t* lower = block;
        block_t* upper = (block_t*)((IADR)block + (IADR)ORDER_BLOCKBYTES(prev_order));
        push_free_block(base, &order_list[prev_order], upper, prev_order);
        push_free_block(base, &order_list[prev_order], lower, prev_order);
    
        --current_order;
    }
}

void queue_joinables(void* base, order_t* order_entry, block_t* block, U8 order)
{
    U32 block_bytes = ORDER_BLOCKBYTES(order);
    U32 block_id = ((IADR)block - (IADR)base) / block_bytes;
    U32 bit_id = block_id / 2;
    if (!is_state_set(order_entry, bit_id))
    {
        U8 is_lower = (block_id + 1) % 2;
        block_t* buddy = block;
        buddy = (block_t*)((IADR)buddy + (block_bytes * is_lower));
        buddy = (block_t*)((IADR)buddy - (block_bytes * (block_id % 2)));

        block_t* lower = block;
        block_t* upper = buddy;
        if (lower > upper)
        {
            std::swap(lower, upper);
        }

        to_listfront(order_entry, upper);
        to_listfront(order_entry, lower);
    }
}

bool head_joinable(order_t* order_entry, U8 order)
{
    U32 order_bytes = ORDER_BLOCKBYTES(order);
    return ((IADR)order_entry->free + order_bytes) == ((IADR)order_entry->free->next);
}

void single_join_step(void* base, order_t* order_list, U8 order, U8 max_order)
{
    // Function requires that there are 
    // at least 2 blocks in list
    order_t* order_entry = &order_list[order];
    if (head_joinable(order_entry, order))
    {
        block_t* lower = order_entry->free;
        order_list[order].free = order_entry->free->next->next;
        order_list[order].free_blocks -= 2;

        U8 next_order = order + 1;
        push_free_block(base, &order_list[next_order], lower, next_order);
        if (next_order < max_order) 
        {
            queue_joinables(base, &order_list[next_order], lower, next_order);
        }
    }
}

void forced_join(void* base, order_t* order_list, U8 order, U8 min_order, U8 max_order)
{
    U32 current_order = min_order;
    while (current_order < max_order)
    {
        order_t* current_entry = &order_list[current_order];
        while (current_entry->free_blocks >= 2 &&
            head_joinable(current_entry, current_order))
        {
            single_join_step(base, order_list, current_order, max_order);
        }
        
        ++current_order;
    }
}

U32 pairs_in_order(U8 n_orders, U8 order)
{
    assert(n_orders >= order);

    // corresponds to (2^max_order / 2^order) / 2
    return POW2(n_orders - order - 1);
}

U16 required_storage_bytes(U32 bits)
{
    return (bits / BYTE_BITS) + ((bits % BYTE_BITS == 0) ? 0 : 1);
}

U32 log2(U32 operand)
{
    if (operand > 0)
    {
        operand--;
        U32 zeros = 0;
        __asm {
            lzcnt eax, operand;
            mov   zeros, eax;
        }
        return (32 - zeros);
    }

    return 0;
}

void init_orders(void* base, order_t* order_list, U8 max_order)
{
    for (U8 i = MIN_ORDER; i < max_order; ++i)
    {
        U32 num_statebits = pairs_in_order(max_order + 1, i);
        U16 num_statebytes = required_storage_bytes(num_statebits);

        order_list[i].free = nullptr;
        order_list[i].pair_state = (BYTE*)malloc(num_statebytes);
        order_list[i].free_blocks = 0;

        for (U16 j = 0; j < num_statebytes; ++j)
        { order_list[i].pair_state[j] = 0; }
    }

    // Set whole heap as free in highest order
    order_list[max_order].free = (block_t*)base;
    order_list[max_order].pair_state = (BYTE*) malloc(1);
    order_list[max_order].free_blocks = 1;
    order_list[max_order].free->prev = nullptr;
    order_list[max_order].free->next = nullptr;

    // Never allow join of topmost block
    *order_list[max_order].pair_state = 1; 
}

void free_orders(order_t* order_list, U8 max_order)
{
    for (U8 i = MIN_ORDER; i <= max_order; ++i)
    { free(order_list[i].pair_state); }
}

void* allocate_block(void* base, U8 align, order_t* order_entry, U8 order)
{
    block_t* block = pop_free_block(base, order_entry, order);

    IADR header_addr = (IADR)block;
    IADR align_addr = header_addr + sizeof(block_header_t);
    IADR data_addr = (align_addr + align) & ~(align - 1);
    IADR align_offset = data_addr - align_addr;
    align_addr = data_addr - sizeof(align);

    ((block_header_t*)header_addr)->order = order;
    *((U8*)align_addr) = align_offset;

    U8 header_size = sizeof(block_header_t);
    return ((BYTE*)data_addr);
}

HeapMemory::HeapMemory(U8 n_orders)
{
    assert(MIN_ORDER >= 1);
    n_orders = n_orders < MIN_ORDER + 1 ? MIN_ORDER +1: n_orders;

    // Overallocating order_list for convenience
    this->max_order  = n_orders - 1;
    this->order_list = (order_t*) malloc(sizeof(order_t) * n_orders);
    this->base       = (BYTE*) malloc(ORDER_BLOCKBYTES(this->max_order));

    // Initialize management structure
    if (this->order_list != nullptr && this->base != nullptr)
    {
        init_orders(this->base, this->order_list, max_order);
    }
    else
    {
        if (this->order_list != nullptr) { free(this->order_list); };
        if (this->base       != nullptr) { free(this->base);       };
    }
}

HeapMemory::~HeapMemory()
{
    free_orders(this->order_list, this->max_order);
    free(this->base);
    free(this->order_list);
}

void* HeapMemory::Alloc(U64 bytes, U8 align)
{
    bytes += sizeof(block_header_t) + align;
    U8 order = (U8)log2(bytes / ORDER_BLOCKBYTES(0));
    order = order < MIN_ORDER ? MIN_ORDER : order;

    // Break up blocks from higher orders if necessary
    ensure_order_block(this->base, this->order_list, this->max_order, order);
    
    // Emergency join lower orders if no space is left
    if(this->order_list[order].free == nullptr && 
       this->order_list[order].free_blocks >= 2)
    { 
        forced_join(this->base, this->order_list, order, MIN_ORDER, this->max_order); 
    }

    if (this->order_list[order].free != nullptr)
    {
        if (this->order_list[order].free_blocks >= 3)
        {
            single_join_step(this->base, order_list, order, max_order);
        }
        return allocate_block(this->base, align, &this->order_list[order], order);
    }

    return nullptr;
}

void HeapMemory::Free(void* addr)
{
    IADR align_addr = (IADR)addr - 1;
    U8 align_offset = *((U8*)align_addr);

    IADR header_addr = (IADR)addr - sizeof(block_header_t) - align_offset;
    block_header_t* header = (block_header_t*)(header_addr);
    U8 order = header->order;
    block_t* block = (block_t*)header;

    push_free_block(base, &order_list[order], block, order);

    if (order < max_order)
    {
        queue_joinables(this->base, &order_list[order], block, order);
    }

    if (order_list[order].free_blocks >= 3)
    {
        single_join_step(this->base, order_list, order, max_order - 1);
    }
}