#pragma once

#include "DrawSystem.h"
#include "Vector3.h"
#include "Matrix4.h"

class Camera
{
public:
    Camera();

    void Destroy();
    void SetDrawSystem(DrawSystem* drawing);
    void SetPosition(Vector3 position);
    void SetYawAngle(float radians);
    void SetPitchAngle(float radians);
    void Update();

private:
    DrawSystem* drawing;
    Matrix4 proj;
    Vector3 pos;
    float yaw;
    float pitch;
    Ref16 gpu_buffer;
};
