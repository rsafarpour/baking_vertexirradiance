#pragma once

#include "DrawSystem.h"
#include "sBVH.h"
#include "StaticGeometry.h"

class DebugRenderer
{
public:
    DebugRenderer(HeapMemory* mem_backend, DrawSystem* drawing);
    ~DebugRenderer();

    void CreateAABB(sBVH* bvh, U32 index);
    void CreateRays(StaticGeometry* geometry, U32 vertex_number, U32 num_rays);
    void DrawAABBs();
    void DrawRays();

private:
    StackMemory memory;
    DrawSystem* drawing;
    Ref16 ref_raymesh;
    Ref16 ref_raygpuprog;
    Ref16 ref_aabbmesh;
    Ref16 ref_aabbgpuprog;
};
