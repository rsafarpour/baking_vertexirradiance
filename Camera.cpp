#include "Camera.h"
#include <cassert>

Camera::Camera()
    : drawing(nullptr),
    pos({0.0f, 0.0f, 0.0f}),
    yaw(0.0f),
    pitch(0.0f)
{
    this->proj = PerspectiveProjectionMatrix(45.0f / 180.0f * 3.1415f, 800.0f / 600.0f, 1.0f, 200.0f);
}

void Camera::Destroy()
{
    this->drawing->FreeDynamicBuffer(this->gpu_buffer);
}

void Camera::SetDrawSystem(DrawSystem* drawing)
{
    assert(drawing != nullptr);
    this->drawing = drawing;

    Matrix4 view = {
        1.0f, 0.0f, 0.0f, 0.0f,    
        0.0f, 1.0f, 0.0f, 0.0f,    
        0.0f, 0.0f, 1.0f, 0.0f,    
        0.0f, 0.0f, 0.0f, 1.0f
    };

    Matrix4 viewproj = mul(&proj, view);
    transpose(&viewproj);

    BufferData bdat = {};
    bdat.bytes = sizeof(Matrix4);
    bdat.data = &viewproj;
    this->gpu_buffer = drawing->UploadDynamicBuffer(&bdat);
}

void Camera::SetPosition(Vector3 position)
{
    this->pos = position;
}
void Camera::SetYawAngle(float radians)
{
    this->yaw = radians;
}
void Camera::SetPitchAngle(float radians)
{
    this->pitch = radians;
}
void Camera::Update()
{
    float cos_t_yaw = cos(this->yaw);
    float sin_t_yaw = sin(this->yaw);
    float cos_t_pitch = cos(this->pitch);
    float sin_t_pitch = sin(this->pitch);

    Matrix4 yaw = {cos_t_yaw, 0.0, sin_t_yaw, 0.0,    0.0, 1.0, 0.0, 0.0,    -sin_t_yaw, 0.0, cos_t_yaw, 0.0,    0.0, 0.0, 0.0, 1.0};
    Matrix4 pitch = {1.0, 0.0, 0.0, 0.0,    0.0, cos_t_pitch, -sin_t_pitch, 0.0,    0.0, sin_t_pitch, cos_t_pitch, 0.0,    0.0, 0.0, 0.0, 1.0};

    Matrix4 view = mul(&pitch, yaw);
    transpose(&view);

    set(&view, 0, 3, getx(this->pos));
    set(&view, 1, 3, gety(this->pos));
    set(&view, 2, 3, getz(this->pos));

    Matrix4 viewproj = mul(&this->proj, view);
    transpose(&viewproj);

    BufferData bdat = {};
    bdat.bytes = sizeof(Matrix4);
    bdat.data = &viewproj;
    this->drawing->UpdateDynamicBuffer(this->gpu_buffer, &bdat);
    this->drawing->SetProjectionMatrix(this->gpu_buffer);
}
