#include "sBVH.h"
#include "search.h"
#include "sorting.h"

#include <stack>

#define POW2(exponent) (1 << exponent)

void eval_root(AABB* new_volumes, U32 num, AABB* root_volume)
{
    Vector3 pmin = root_volume->min;
    Vector3 pmax = root_volume->max;
    for (U32 i = 0; i < num; ++i)
    {
        pmin = min(new_volumes[i].min, pmin);
        pmax = max(new_volumes[i].max, pmax);
    }

    root_volume->min = pmin;
    root_volume->max = pmax;
}

U32 morton3D(Vector3 v)
{
    // Based on https://devblogs.nvidia.com/thinking-parallel-part-iii-tree-construction-gpu/
    U32 x = (U32)getx(v);
    x = (x * 0x00010001u) & 0xFF0000FFu;
    x = (x * 0x00000101u) & 0x0F00F00Fu;
    x = (x * 0x00000011u) & 0xC30C30C3u;
    x = (x * 0x00000005u) & 0x49249249u;

    U32 y = (U32)gety(v);
    y = (y * 0x00010001u) & 0xFF0000FFu;
    y = (y * 0x00000101u) & 0x0F00F00Fu;
    y = (y * 0x00000011u) & 0xC30C30C3u;
    y = (y * 0x00000005u) & 0x49249249u;

    U32 z = (U32)getz(v);
    z = (z * 0x00010001u) & 0xFF0000FFu;
    z = (z * 0x00000101u) & 0x0F00F00Fu;
    z = (z * 0x00000011u) & 0xC30C30C3u;
    z = (z * 0x00000005u) & 0x49249249u;

    U32 xx = (x << 2);
    U32 yy = (y << 1);
    U32 zz = z;
    U32 code = (xx | yy | zz);
    return code;
}

void calc_mortoncodes(AABB root_volume, Vector3* centroids, U32 num, U32* morton_codes)
{
    for (U32 i = 0; i < num; ++i)
    {
        Vector3 normalized = ihadamard(centroids[i], (root_volume.max - root_volume.min));
        normalized = min(max({0.0f, 0.0f, 0.0f}, scale(1024.0, normalized)), {1023.0f, 1023.0f, 1023.0f});
        morton_codes[i] = morton3D(normalized);
    }
}

void linear_fill(U32* array, U32 num)
{
    for (U32 i = 0; i < num; ++i)
    { array[i] = i; }
}

AABB aggregate_aabbs(AABB* aabbs, U32* prim_ids, U32 range_low, U32 range_high)
{
    if (range_low == range_high)
    { return {}; }

    AABB compound;
    compound.min = aabbs[prim_ids[range_low]].min;
    compound.max = aabbs[prim_ids[range_low]].max;
    for (U32 i = range_low; i < range_high; ++i)
    {
        compound.min = min(compound.min, aabbs[prim_ids[i]].min);
        compound.max = max(compound.max, aabbs[prim_ids[i]].max);
    }

    return compound;
}

U32 build_hierarchy(AABB* volumes, U32* morton_codes, U32* prim_ids, U8 bit, U32 range_low, U32 range_high, AABB* hierarchy, U32* skip_vals, U32* bases, U32* ranges)
{
    if (bit < 31)
    {
        U32 split_index = range_high;
        while (split_index >= range_high && morton_codes[range_low] != morton_codes[range_high - 1])
        {
            split_index = radix_upperbound(morton_codes, 31 - bit, range_low, range_high);
            ++bit;
        }

        U32 num_subvols = 0;
        if (range_low < split_index &&
            range_high > split_index)
        {
            num_subvols += build_hierarchy(volumes, morton_codes, prim_ids, bit, range_low, split_index, &hierarchy[1], &skip_vals[1], &bases[1], &ranges[1]);
            num_subvols += build_hierarchy(volumes, morton_codes, prim_ids, bit, split_index, range_high, &hierarchy[num_subvols + 1], &skip_vals[num_subvols + 1], &bases[num_subvols + 1], &ranges[num_subvols + 1]);
        }

        hierarchy[0] = aggregate_aabbs(volumes, prim_ids, range_low, range_high);
        skip_vals[0] = num_subvols;
        bases[0]     = range_low;
        ranges[0]    = range_high - range_low;
        return num_subvols + 1;
    }

    return 0;
}

sBVH::sBVH(HeapMemory* mem_backend)
  : vol_memory(mem_backend, 4 * 1024),
    work_memory(mem_backend, 8 * 1024),
    root_volume({}),
    volumes(nullptr),
    skip_vals(nullptr),
    prim_ids(nullptr),
    volume_base(nullptr),
    volume_range(nullptr),
    num_prims(0)
{
    assert(mem_backend != nullptr);
    this->volumes = (AABB*)this->vol_memory.Alloc(0, sizeof(Vector3)); // Fetch base pointer
}

void sBVH::GetVolume(U32 index, AABB* aabb)
{
    if (index < this->num_volumes)
    { *aabb = this->volumes[index]; }
}

void sBVH::RaycastCandidates(Ray r, U32 max_num, U32* candidate_prims, U32* out_num)
{
    assert(candidate_prims != nullptr);
    assert(out_num != nullptr);

    U32 bvh_index = 0;
    *out_num = 0;
    while (bvh_index < this->num_volumes && *out_num < max_num)
    {
        bool contains_origin = is_inside(this->volumes[bvh_index], r.o);
        bool intersected = intersects(&this->volumes[bvh_index], r);
    
        if (!(contains_origin || intersected))
        { 
            bvh_index += this->skip_vals[bvh_index]; 
        }
        else if (this->skip_vals[bvh_index] == 0) // Only submit lowest level volumes
        {
            U32 base = this->volume_base[bvh_index];
            U32 range = base + this->volume_range[bvh_index];
            for (U32 i = base; i < range && *out_num < max_num; ++i)
            {
                candidate_prims[*out_num] = this->prim_ids[i];
                ++(*out_num);
            }
        }
    
        bvh_index++;
    }
}

void sBVH::InsertPrimitives(Vector3* vertices, U32* indices, U32 num_prims)
{
    this->vol_memory.Clear(); // Delete old hierarchy
    this->volumes = nullptr;  // Mark hierarchy as invalid

    if (num_prims == 0)
    { return; }

    AABB* aabbs = (AABB*)this->work_memory.Alloc(sizeof(AABB) * num_prims, sizeof(Vector3));
    if (aabbs != nullptr)
    {
        for (U32 i = 0; i < num_prims; ++i)
        {
            Vector3 a = vertices[indices[i * 3 + 0]];
            Vector3 b = vertices[indices[i * 3 + 1]];
            Vector3 c = vertices[indices[i * 3 + 2]];
            aabbs[i] = AABB{
                min(a, min(b, c)),
                max(a, max(b, c)),
            };
        }

        this->num_prims += num_prims;
    }

    eval_root(aabbs, num_prims, &this->root_volume);

    Vector3* centroids = (Vector3*)work_memory.Alloc(sizeof(Vector3)*this->num_prims, sizeof(Vector3));
    U32* morton_codes = (U32*)work_memory.Alloc(sizeof(U32)*this->num_prims, sizeof(U32));
    calc_centroids(aabbs, this->num_prims, centroids);
    calc_mortoncodes(this->root_volume, centroids, this->num_prims, morton_codes);

    this->prim_ids = (U32*)vol_memory.Alloc(sizeof(U32)*this->num_prims, sizeof(U32));
    linear_fill(prim_ids, this->num_prims);
    radix_sort(&this->work_memory, morton_codes, prim_ids, this->num_prims);

    this->volumes = (AABB*)this->vol_memory.Alloc(sizeof(AABB) * num_prims * 2, sizeof(Vector3));
    this->skip_vals = (U32*)this->vol_memory.Alloc(sizeof(U32) * num_prims * 2, sizeof(U32));
    this->volume_base = (U32*)this->vol_memory.Alloc(sizeof(U32) * num_prims * 2, sizeof(U32));
    this->volume_range = (U32*)this->vol_memory.Alloc(sizeof(U32) * num_prims * 2, sizeof(U32));
    this->num_volumes = build_hierarchy(aabbs, morton_codes, prim_ids, 0, 0, this->num_prims, this->volumes, this->skip_vals, this->volume_base, this->volume_range);

    this->work_memory.Clear();
};
