#pragma once

#include "types.h"
#include "HeapMemory.h"

class PoolMemory
{
public:
    PoolMemory(HeapMemory* memory, U32 object_size, U32 num_objects);
    ~PoolMemory();

    void* Alloc();
    void  Free(void* ptr);

private:
    HeapMemory* memory;
    void*       base;
    U32         head;
    U32         num_free;
    U32         num_init;
    U32         object_size;
};