#pragma once

#include <xmmintrin.h>

// Row major 3x3 matrix
struct Matrix3
{
    __m128 _0;
    __m128 _1;
    __m128 _2;
};

inline Vector3 mul(const Matrix3 a, const Vector3 b)
{
    __m128 tmp = _mm_mul_ps(a._0, b);
    tmp = _mm_hadd_ps(tmp, tmp);
    tmp = _mm_hadd_ps(tmp, tmp);
    float x = _mm_cvtss_f32(tmp);
    
    tmp = _mm_mul_ps(a._1, b);
    tmp = _mm_hadd_ps(tmp, tmp);
    tmp = _mm_hadd_ps(tmp, tmp);
    float y = _mm_cvtss_f32(tmp);
    
    tmp = _mm_mul_ps(a._2, b);
    tmp = _mm_hadd_ps(tmp, tmp);
    tmp = _mm_hadd_ps(tmp, tmp);
    float z = _mm_cvtss_f32(tmp);

    return _mm_set_ps(0.0f, z, y, x);
}

inline void transpose(Matrix3* m)
{
    _MM_TRANSPOSE4_PS(m->_0, m->_1, m->_2, _mm_setzero_ps());
}