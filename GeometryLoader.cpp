#include "GeometryLoader.h"
#include <stdio.h>
#include <vector>
#include "units.h"
#include "FileIO.h"

#define NUM_VERTEXFLOATS 12
#define NUM_TRIVERTICES   3

#define MAX_IDCHAR 30

#define IS_DIGIT(d) (d <= 0x39 && d >= 0x30)
#define DIGIT_TO_INT(d) (d - '0')

void slice_xyz(float* vertices, U8 offset, U8 stride, U32 num, Vector3* out)
{    
    for (U32 i = 0; i < num; ++i)
    {
        U32 base_index = i * stride + offset;
        out[i + 0] = {vertices[base_index + 0], vertices[base_index + 1], vertices[base_index + 2]};
    }    
}

void slice_rgb(float* vertices, U8 offset, U8 stride, U32 num, ColorRGBA_F* out)
{
    for (U32 i = 0; i < num; ++i)
    {
        U32 base_index = i * stride + offset;
        out[i + 0] = {vertices[base_index + 0], vertices[base_index + 1], vertices[base_index + 2]};
    }
}

void read_id(char* content, U64* file_pos, unsigned char* id, U32 max_length)
{
    max_length -= 1;

    U32 index = 0;
    unsigned char c = content[(*file_pos)++];;
    while (c != '\n' && index < max_length)
    {
        id[index++] = c;
        c = content[(*file_pos)++];
    }

    id[index] = '\0';

    // Scan past end of id without saving it
    while (c != '\n')
    {
        c = content[(*file_pos)++];
    }
}

bool read_integer(char* content, U64* file_pos, U32* num)
{
    unsigned char digit = content[*file_pos];
    (*file_pos)++;

    *num = 0;
    while (digit != '\n')
    {
        if (IS_DIGIT(digit))
        {
            *num *= 10;
            *num += DIGIT_TO_INT(digit);
        }
        else
        {
            return false;
        }
        digit = content[*file_pos];
        (*file_pos)++;
    }
    return true;
}

void read_float(char* content, U64* file_pos, float* num)
{
    // Vertex format is fixed to 3x position, 3x normal, 4x rgba color
    U8 index = 0;
    U64 end_pos = *file_pos;
    unsigned char c = content[end_pos++];
    while (c != ' ' && c != '\n' && c != EOF)
    {
        c = content[end_pos++];
    }

    content[end_pos - 1] = '\0';
    *num = strtof(&content[*file_pos], nullptr);
    content[end_pos - 1] = c;

    *file_pos = end_pos;
}

void read_vertex(char* content, U64* file_pos, float* vertex, U8 floats)
{
    // Vertex format is fixed to 3x position, 3x normal, 3x albedo 3x emittance
    U8 index = 0;
    while (index < floats)
    {
        read_float(content, file_pos, &vertex[index++]);
        while (content[*file_pos] == ' ' || content[*file_pos] == '\n')
        { ++(*file_pos); }
    }
}

void read_tri(char* content, U64* file_pos, U32* indices, U32 num_indices)
{
    U8 index = 0;
    while (index < num_indices)
    {
        read_integer(content, file_pos, &indices[index++]);
        while (content[*file_pos] == ' ' || content[*file_pos] == '\n')
        { ++(*file_pos); }
    }
}






GeometryLoader::GeometryLoader(HeapMemory* mem_backend)
    : memory(mem_backend, 1024 * 100) // 100kB
{}

void GeometryLoader::Load(StaticGeometry* geometry, const char* filename)
{
    U64 filesize = PokeFilesize(filename);
    
    char* content = (char*) this->memory.Alloc(filesize);
    U64 content_size = ReadFile(filename, content, filesize);
    
    if(content_size == 0)
    { return; }

    U32 num_vertices = 0; 
    U32 num_indices = 0;
    U64 file_pos = 0;
    MeshData data = {};
    if (read_integer(content, &file_pos, &num_vertices) && read_integer(content, &file_pos, &num_indices))
    {
        float* vertices = (float*) memory.Alloc(sizeof(float) * NUM_VERTEXFLOATS * num_vertices, sizeof(float));
        U32*   indices  = (U32*)   memory.Alloc(sizeof(U32) * NUM_TRIVERTICES * num_indices, sizeof(U32));

        if (vertices == nullptr || indices == nullptr)
        { 
            this->memory.Clear();
            return; 
        }

        unsigned char object_id[MAX_IDCHAR];
        unsigned char line_id = content[file_pos++];
        const U32 max_vertidx = num_vertices * NUM_VERTEXFLOATS;
        const U32 max_indcidx = num_indices;
        U32 vert_idx = 0;
        U32 indc_idx = 0;
        while (file_pos < content_size)
        {
            file_pos++; // Skip space
            switch (line_id)
            {
            case 'o':
                read_id(content, &file_pos, object_id, MAX_IDCHAR);
                break;
            case 'v':
                if (vert_idx < max_vertidx)
                {
                    read_vertex(content, &file_pos, &vertices[vert_idx], NUM_VERTEXFLOATS);
                    vert_idx += NUM_VERTEXFLOATS;
                }
                break;
            case 'f':
                if (indc_idx < max_indcidx)
                {
                    read_tri(content, &file_pos, &indices[indc_idx], NUM_TRIVERTICES);
                    indc_idx += NUM_TRIVERTICES;
                }
                break;
            default:
                this->memory.Clear();
                return;
            }
        
            line_id = content[file_pos++];
        }

        data.vertices = vertices;
        data.indices = indices;
        data.num_vertices = num_vertices;
        data.num_indices = num_indices;
    }

    if (geometry != nullptr)
    { 
        Vector3* positions = (Vector3*)this->memory.Alloc(sizeof(Vector3) * num_vertices, sizeof(Vector3));
        Vector3* normals = (Vector3*) this->memory.Alloc(sizeof(Vector3) * num_vertices, sizeof(Vector3));
        ColorRGBA_F* albedo = (ColorRGBA_F*)this->memory.Alloc(sizeof(ColorRGBA_F) * num_vertices, sizeof(ColorRGBA_F));
        ColorRGBA_F* emittance = (ColorRGBA_F*) this->memory.Alloc(sizeof(ColorRGBA_F) * num_vertices, sizeof(ColorRGBA_F));
        slice_xyz(data.vertices, 0, NUM_VERTEXFLOATS, num_vertices, positions);
        slice_xyz(data.vertices, 3, NUM_VERTEXFLOATS, num_vertices, normals);
        slice_rgb(data.vertices, 6, NUM_VERTEXFLOATS, num_vertices, albedo);
        slice_rgb(data.vertices, 9, NUM_VERTEXFLOATS, num_vertices, emittance);

        geometry->InsertPrimitives(positions, normals, albedo, emittance, num_vertices, data.indices, num_indices);
    }

    this->memory.Clear();
}