cbuffer ProjMatrix : register(b0)
{
	float4x4 _proj;
}

struct VertexIn
{
    float3 position   : POSITION;
    float3 normal     : NORMAL;
    float3 albedo     : ALBEDO;
    float3 irradiance : IRRADIANCE;
    float3 emittance  : EMITTANCE;
};

struct VertexOut
{
    float3 albedo     : ALBEDO;
    float3 irradiance : IRRADIANCE;
    float3 emittance  : EMITTANCE;
    float4 position : SV_POSITION;
};

VertexOut main(VertexIn i, uint id :SV_VertexID)
{
	float4 position = float4(i.position, 1.0f);

    VertexOut o;
    o.position = mul(_proj, position);

    o.albedo     = i.albedo;
	o.irradiance = i.irradiance;
	o.emittance  = i.emittance;
	return o;
}