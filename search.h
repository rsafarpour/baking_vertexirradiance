#pragma once

#include "types.h"

U32 radix_upperbound(U32* keys, U32 bit, U32 range_low, U32 range_high);
