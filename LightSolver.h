#pragma once

#include "StaticGeometry.h"

class LightSolver
{
public:
    LightSolver(HeapMemory* mem_backend);
    void SolveGeometryRadiance(StaticGeometry* geom, U32 bounces, ColorRGBA_F* irradiance);

private:
    StackMemory memory;
};
