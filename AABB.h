#pragma once

#include "types.h"
#include "Vector3.h"
#include "Ray.h"

#define AABB_EPSILON 0.0001f

struct AABB
{
    Vector3 min;
    Vector3 max;
};

inline bool is_inside(AABB aabb, Vector3 p)
{
    __m128 offset = _mm_set_ps1(AABB_EPSILON);
    __m128 offset_min = p + offset;
    __m128 offset_max = p - offset;

    __m128 lt = _mm_cmplt_ps(offset_min, aabb.min);
    __m128 gt = _mm_cmpgt_ps(offset_max, aabb.max);

    return !(_mm_movemask_ps(lt) || _mm_movemask_ps(gt));
}

inline bool intersects(AABB* aabb, Ray ray)
{
    Vector3 normal_xplane{1.0f, 0.0f, 0.0f};
    Vector3 normal_yplane{0.0f, 1.0f, 0.0f};
    Vector3 normal_zplane{0.0f, 0.0f, 1.0f};

    float t_top = dot(aabb->min - ray.o, normal_xplane) / dot(ray.d, normal_xplane);
    float t_bottom = dot(aabb->max - ray.o, normal_xplane) / dot(ray.d, normal_xplane);
    float t_left = dot(aabb->min - ray.o, normal_yplane) / dot(ray.d, normal_yplane);
    float t_right = dot(aabb->max - ray.o, normal_yplane) / dot(ray.d, normal_yplane);
    float t_back = dot(aabb->min - ray.o, normal_zplane) / dot(ray.d, normal_zplane);
    float t_front = dot(aabb->max - ray.o, normal_zplane) / dot(ray.d, normal_zplane);

    return (is_inside(*aabb, scale(t_top, ray.d) + ray.o) ||
        is_inside(*aabb, scale(t_bottom, ray.d) + ray.o) ||
        is_inside(*aabb, scale(t_left, ray.d) + ray.o) ||
        is_inside(*aabb, scale(t_right, ray.d) + ray.o) ||
        is_inside(*aabb, scale(t_top, ray.d) + ray.o) ||
        is_inside(*aabb, scale(t_back, ray.d) + ray.o) ||
        is_inside(*aabb, scale(t_front, ray.d) + ray.o));
}

inline void calc_centroids(AABB* aabbs, U32 num, Vector3* centroid)
{
    for (U32 i = 0; i < num; ++i)
    {
        centroid[i] = scale(0.5f, aabbs[i].min + aabbs[i].max);
    }
}