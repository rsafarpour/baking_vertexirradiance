#pragma once
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")

#define NOMINMAX

#include "BufferData.h"
#include "GPUProgramData.h"
#include "MeshData.h"
#include "types.h"
#include "PoolMemory.h"

#include <d3d11.h>
#include <windows.h>
#include <vector>

#define MAX_IDS 32

class IDRedirect
{
public:
    IDRedirect();

    Ref16 insert(void* addr);
    void  remove(U32 id);
    void* translate(U32 id);
    U16   in_use();

private:
    U16   size_free;
    Ref16 free[MAX_IDS];
    void* addr[MAX_IDS];
};

struct Mesh
{
    ID3D11Buffer* vbuffer;
    ID3D11Buffer* ibuffer;
    U32           num_indices;
    U32           vertex_stride;
};

struct GPUProgram
{
    ID3D11InputLayout*  layout;
    ID3D11VertexShader* vs;
    ID3D11PixelShader*  ps;
};

class DrawSystem
{
public:
    DrawSystem(HeapMemory* mem_backend);
    ~DrawSystem();

    void Clear(float* color);
    void Present();
    void Setup(HWND hwnd);

    void DrawTriangles(Ref16 ref_mesh);
    void DrawLines(Ref16 ref_mesh);
    void SetGPUProgram(Ref16 ref_program);
    void SetProjectionMatrix(Ref16 ref_buffer);

    void UpdateDynamicBuffer(Ref16 ref_buffer, BufferData* data);
    Ref16 UploadDynamicBuffer(BufferData* data);
    Ref16 UploadMesh(U32 vertex_stride, MeshData* mesh);
    Ref16 UploadGPUProgram(const D3D11_INPUT_ELEMENT_DESC* layout, U32 layout_elements, GPUProgramData* program_data);

    void FreeMesh(Ref16 ref_mesh);
    void FreeGPUProgram(Ref16 ref_program);
    void FreeDynamicBuffer(Ref16 ref_buffer);

private:
    IDRedirect ids_gpuprograms;
    IDRedirect ids_meshes;
    IDRedirect ids_dbuffers;
    PoolMemory mem_gpuprograms;
    PoolMemory mem_meshes;

    ID3D11Texture2D*           ds_texture;
    ID3D11DepthStencilState*   ds_state;
    ID3D11DepthStencilView*    dsv;

    ID3D11BlendState*          blend_state;
    ID3D11RasterizerState*     rasterizer_state;
    ID3D11RenderTargetView*    rtv;
};
