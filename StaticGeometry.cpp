#include "StaticGeometry.h"

#include "Matrix2.h"
#include "Matrix3.h"
#include "PolarCoordinates.h"
#include <cfloat>
#include <random>

#define INVALID_BARYCENTER Vector3{-1.0, -1.0, -1.0}

#define NUM_PRIMS (this->num_inds / 3U)

Vector3 get_barycenter(Ray* ray, Vector3 a, Vector3 b, Vector3 c, float* distance)
{
    *distance = FLT_MAX;

    Vector3 ca = a - c;
    Vector3 cb = b - c;
    Vector3 n = normalized(cross(ca, cb));

    float t = dot(a - ray->o, n) / dot(ray->d, n);

    if (dot(ray->d, n) != 0 && t > 0.0f)
    {
        Vector3 p = scale(t, ray->d) + ray->o;

        Vector3 v0 = b - a, v1 = c - a, v2 = p - a;
        float d00 = dot(v0, v0);
        float d01 = dot(v0, v1);
        float d11 = dot(v1, v1);
        float d20 = dot(v2, v0);
        float d21 = dot(v2, v1);
        float denom = d00 * d11 - d01 * d01;
        float v = (d11 * d20 - d01 * d21) / denom;
        float w = (d00 * d21 - d01 * d20) / denom;
        float u = 1.0f - v - w;

        if (v >= 0.0f && v <= 1.0 &&
            w >= 0.0f && w <= 1.0 &&
            u >= 0.0f && u <= 1.0)
        {
            *distance = norm(ray->o - p);
            return {u, v, w};
        }
    }

    return INVALID_BARYCENTER;
}

StaticGeometry::StaticGeometry(HeapMemory* mem_backend, sBVH* bvh)
  : vertex_memory(mem_backend, 8*1024),
    index_memory(mem_backend, 4*1024),
    mem_backend(mem_backend),
    num_verts(0),
    num_inds(0),
    bvh(bvh)
{
    assert(bvh != nullptr);

    this->vertices = (VertexAttrs*)this->vertex_memory.Alloc(0, sizeof(Vector3)); // Fetch base pointer
    this->indices  = (U32*) this->index_memory.Alloc(0, sizeof(U32)); // Fetch base pointer
}

U32 StaticGeometry::GetNumIndices()
{
    return this->num_inds;
}

U32 StaticGeometry::GetNumVertices()
{
    return this->num_verts;
}

void StaticGeometry::InsertPrimitives(Vector3* pos, Vector3* normal, ColorRGBA_F* albedo, ColorRGBA_F* emittance, U32 num_verts, U32* indices, U32 num_indices)
{
    VertexAttrs* verts = (VertexAttrs*)this->vertex_memory.Alloc(sizeof(VertexAttrs) * num_verts, sizeof(Vector3));
    U32* inds = (U32*) this->vertex_memory.Alloc(sizeof(U32) * num_verts, sizeof(U32));

    if (num_indices % 3 == 0 &&
        verts != nullptr &&
        inds != nullptr)
    {
        for (U32 i = 0; i < num_verts; ++i)
        {
            verts[i].albedo = albedo[i];
            verts[i].emittance = emittance[i];
            verts[i].position = pos[i];
            verts[i].normal = normal[i];
        }

        for (U32 i = 0; i < num_indices; ++i)
        {
            this->indices[i] = indices[i];
        }

        this->bvh->InsertPrimitives(pos, indices, num_indices / 3);
        this->num_verts += num_verts;
        this->num_inds += num_indices;
    }
}

void StaticGeometry::Raycast(Vector3 position, Vector3* dirs, U32 num_rays, U32* prim_indices, Vector3* bary_coords)
{
    Ray r = {position, dirs[0]};

    StackMemory candidate_memory(this->mem_backend, sizeof(U32)*NUM_PRIMS);
    U32 num_prims = this->num_inds / 3U;
    for (U32 i = 0; i < num_rays; ++i)
    {
        r.d = dirs[i];

        U32 num_candidates = 0;
        U32* candidates = (U32*)candidate_memory.Alloc(sizeof(U32)*NUM_PRIMS, sizeof(U32));
        this->bvh->RaycastCandidates(r, NUM_PRIMS, candidates, &num_candidates);
        
        float shortest_dist = FLT_MAX;
        prim_indices[i] = NUM_PRIMS;
        for (U32 j = 0; j < num_candidates; ++j)
        {
            float distance;
            U32 prim_index = candidates[j];
            Vector3 a = this->vertices[this->indices[3 * prim_index + 0]].position;
            Vector3 b = this->vertices[this->indices[3 * prim_index + 1]].position;
            Vector3 c = this->vertices[this->indices[3 * prim_index + 2]].position;
        
            Vector3 coords = get_barycenter(&r, a, b, c, &distance);
        
            if (coords != INVALID_BARYCENTER &&
                distance < shortest_dist && 
                distance > 0.0001) // prevents self intersection
            {
                shortest_dist = distance;
                prim_indices[i] = prim_index;
                bary_coords[i] = coords;
            }
        }

        candidate_memory.Clear();
    }
}

void StaticGeometry::InterpolateAlbedo(U32* prim_indices, Vector3* bary_coords, ColorRGBA_F* last_bounce, U32 num_points, ColorRGBA_F* colors)
{
    for (U32 i = 0; i < num_points; ++i)
    {
        if (prim_indices[i] < NUM_PRIMS)
        {
            ColorRGBA_F albedo = bary_interpolate(bary_coords[i],
                this->vertices[this->indices[prim_indices[i] * 3 + 0]].albedo,
                this->vertices[this->indices[prim_indices[i] * 3 + 1]].albedo,
                this->vertices[this->indices[prim_indices[i] * 3 + 2]].albedo);

            colors[i] = hadamard(albedo, bary_interpolate(bary_coords[i],
                last_bounce[this->indices[prim_indices[i] * 3 + 0]],
                last_bounce[this->indices[prim_indices[i] * 3 + 1]],
                last_bounce[this->indices[prim_indices[i] * 3 + 2]]));
        }
        else
        {
            colors[i] = {0.0f, 0.0f, 0.0f};
        }
    }
}

void StaticGeometry::InterpolateEmittance(U32* prim_indices, Vector3* bary_coords, U32 num_points, ColorRGBA_F* colors)
{
    for (U32 i = 0; i < num_points; ++i)
    {
        if (prim_indices[i] < NUM_PRIMS)
        {
            colors[i] = bary_interpolate(bary_coords[i], 
                this->vertices[this->indices[prim_indices[i] * 3 + 0]].emittance,
                this->vertices[this->indices[prim_indices[i] * 3 + 1]].emittance,
                this->vertices[this->indices[prim_indices[i] * 3 + 2]].emittance);
        }
        else
        {
            colors[i] = {0.0f, 0.0f, 0.0f};
        }
    }
}

void StaticGeometry::GetNormals(U32* vert_indices, Vector3* normals, U32 num)
{
    for (U32 i = 0; i < num; ++i)
    {
        if (vert_indices[i] < this->num_verts)
        {
            normals[i] = this->vertices[vert_indices[i]].normal;
        }
    }
}

void StaticGeometry::GetPositions(U32* vert_indices, Vector3* positions, U32 num)
{
    for (U32 i = 0; i < num; ++i)
    {
        if (vert_indices[i] < this->num_verts)
        {
            positions[i] = this->vertices[vert_indices[i]].position;
        }
    }
}

void StaticGeometry::GetAlbedo(U32* vert_indices, Vector3* albedo, U32 num)
{
    for (U32 i = 0; i < num; ++i)
    {
        if (vert_indices[i] < this->num_verts)
        {
            albedo[i] = this->vertices[vert_indices[i]].albedo;
        }
    }
}

void StaticGeometry::GetEmittance(U32* vert_indices, Vector3* emittance, U32 num)
{
    for (U32 i = 0; i < num; ++i)
    {
        if (vert_indices[i] < this->num_verts)
        {
            emittance[i] = this->vertices[vert_indices[i]].emittance;
        }
    }
}

void StaticGeometry::GetIndices(U32* indices, U32 num)
{
    for (U32 i = 0; i < num; ++i)
    {
        if (i < this->num_inds)
        {
            indices[i] = this->indices[i];
        }
    }
}