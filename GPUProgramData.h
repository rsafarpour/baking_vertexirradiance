#pragma once

#include "types.h"

struct ShaderBytecode
{
    void* ptr;
    U32   bytes;
};

struct GPUProgramData
{
    ShaderBytecode vs_bytecode;
    ShaderBytecode ps_bytecode;
};
