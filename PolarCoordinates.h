#pragma once

#include "Vector3.h"
#include <immintrin.h>

namespace PolarCoordinates
{
    inline Vector3 ToCartesian(Vector3 polar_coords)
    {
        float components[4];
        get_comps(polar_coords, components);

        return {
            components[0] * cos(components[1]),
            components[0] * sin(components[1])

        };
    }

    inline Vector3 ToPolar(Vector3 cartesian)
    {
        float components[4];
        get_comps(cartesian, components);

        return {
            sqrt(components[0] * components[0] + components[1] * components[1]),
            atan2(components[1], components[0])
        };
    }
}
