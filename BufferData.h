#pragma once

#include <d3d11.h>
#include "types.h"

struct BufferData
{
    void* data;
    U16   bytes;
};
