#include "sorting.h"

void build_histograms(U32* keys, U8 bit, U32 num, U32* zeros, U32* ones)
{
    for (U32 i = 0; i < num; ++i)
    {
        U32 bittest = (keys[i] >> bit) & 1;
        *ones += bittest;
        *zeros += (1 - bittest);
    }
}

void build_positions(U32* keys, U8 bit, U32 prefix0, U32 prefix1, U32 num, U32* positions)
{
    U32 pos_zero = prefix0;
    U32 pos_one = prefix1;
    for (U32 i = 0; i < num; ++i)
    {
        if (((keys[i] >> bit) & 1) == 1)
        {
            positions[i] = pos_one++;
        }
        else
        {
            positions[i] = pos_zero++;
        }
    }
}

void move_elements(U32* keys, U32* values, U8 bit, U32 zeros, U32* positions, U32 num, U32* sorted_keys, U32* sorted_values)
{
    for (U32 i = 0; i < num; ++i)
    {
        U32 offset = ((keys[i] >> bit) & 1) * zeros;
        sorted_keys[offset + positions[i]] = keys[i];
        sorted_values[offset + positions[i]] = values[i];
    }
}

void radix_sort(StackMemory* mem, U32* keys, U32* values, U32 num)
{
    void* tos = mem->GetTop();

    U32* keys_sortedA = keys;
    U32* keys_sortedB = (U32*)mem->Alloc(sizeof(U32)*num, sizeof(U32));
    U32* vals_sortedA = values;
    U32* vals_sortedB = (U32*)mem->Alloc(sizeof(U32)*num, sizeof(U32));
    for (U8 bit = 0; bit < 32; ++bit)
    {
        U32 zeros = 0;
        U32 ones = 0;
        U32* positions = (U32*)mem->Alloc(sizeof(U32)*num, sizeof(U32));
        build_histograms(keys_sortedA, bit, num, &zeros, &ones);
        build_positions(keys_sortedA, bit, 0, 0, num, positions);
        move_elements(keys_sortedA, vals_sortedA, bit, zeros, positions, num, keys_sortedB, vals_sortedB);

        U32* tmp = keys_sortedA;
        keys_sortedA = keys_sortedB;
        keys_sortedB = tmp;

        tmp = vals_sortedA;
        vals_sortedA = vals_sortedB;
        vals_sortedB = tmp;
    }

    if (keys_sortedA != keys)
    {
        for (U32 i = 0; i < num; ++i)
        { keys[i] = keys_sortedA[i]; }
    }

    mem->SetTop(tos);
}