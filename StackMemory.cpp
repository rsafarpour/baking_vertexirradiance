#include "StackMemory.h"

StackMemory::StackMemory(HeapMemory* memory, U64 bytes)
  : memory(memory)
{
    this->base = this->memory->Alloc(bytes);
    this->top = this->base;
    this->free = bytes;

    assert(base != nullptr);
}

StackMemory::~StackMemory()
{
    if(this->base != nullptr)
    { memory->Free(this->base); }
}

void* StackMemory::Alloc(U64 bytes, U8 align)
{
    if (this->base != nullptr && 
        bytes <= this->free)
    {
        void* addr = this->top;
        this->free = this->free - bytes;
        this->top = (void*)((IADR)this->top + bytes);

        IADR base_addr = (IADR)addr;
        IADR aligned_addr = (base_addr + align - 1) & ~(align - 1);

        return ((void*)aligned_addr);
    }
    
    return nullptr;
}

void* StackMemory::GetTop()
{
    return this->top;
}

void StackMemory::SetTop(void* ptr)
{
    if (ptr > this->base && ptr < this->top)
    {
        this->free = this->free + ((IADR)this->top - (IADR)ptr);
        this->top = ptr;
    }
}

void StackMemory::Clear()
{
    this->free = this->free + ((IADR)this->top - (IADR)this->base);
    this->top = this->base;
}