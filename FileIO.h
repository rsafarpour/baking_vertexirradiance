#pragma once

#include "types.h"

U64 PokeFilesize(const char* filename);
U64 ReadFile(const char* filename, char* dst, U64 dst_size);
