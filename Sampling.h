#pragma once

#include "types.h"
#include "Vector3.h"

void HemisphericalSamples(Vector3 up, U32 num_dirs, Vector3* dirs);
