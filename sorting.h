#pragma once

#include "types.h"
#include "StackMemory.h"

void radix_sort(StackMemory* mem, U32* keys, U32* values, U32 num);