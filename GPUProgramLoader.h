#pragma once

#include "DrawSystem.h"
#include "GPUProgramData.h"
#include "StackMemory.h"

class GPUProgramLoader
{
public:
    GPUProgramData Load(StackMemory* stack,
        const char* vs_path,
        const char* ps_path);
   
};
