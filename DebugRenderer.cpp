#include "DebugRenderer.h"
#include "Sampling.h"
#include "VertexLayouts.h"
#include "GPUProgramLoader.h"

DebugRenderer::DebugRenderer(HeapMemory* mem_backend, DrawSystem* drawing)
  : memory(mem_backend, 1024 * 64),
    drawing(drawing),
    ref_aabbmesh(REF16_INVALID),
    ref_aabbgpuprog(REF16_INVALID),
    ref_raymesh(REF16_INVALID),
    ref_raygpuprog(REF16_INVALID)
{
    assert(drawing != nullptr);
}

DebugRenderer::~DebugRenderer()
{
    if (ref_raymesh != REF16_INVALID)
    {
        this->drawing->FreeMesh(this->ref_raymesh);
        this->drawing->FreeGPUProgram(this->ref_raygpuprog);
    }

    if (ref_aabbmesh != REF16_INVALID)
    {
        this->drawing->FreeMesh(this->ref_aabbmesh);
        this->drawing->FreeGPUProgram(this->ref_aabbgpuprog);
    }
}

void DebugRenderer::CreateAABB(sBVH* bvh, U32 index)
{
    if (ref_aabbmesh != REF16_INVALID)
    {
        this->drawing->FreeMesh(this->ref_aabbmesh);
        this->drawing->FreeGPUProgram(this->ref_aabbgpuprog);
    }

    U32 num_aabbs = 0;

    void* tos = this->memory.GetTop();
    AABB aabbs[1];
    bvh->GetVolume(index, aabbs);

    float* vertdata = (float*)memory.Alloc(DebugRays::VertexStride * num_aabbs * 8, sizeof(float));
    U32* inddata = (U32*)memory.Alloc(sizeof(U32) * num_aabbs * 24, sizeof(U32));

    vertdata[ 0] = getx(aabbs[0].min);
    vertdata[ 1] = gety(aabbs[0].min);
    vertdata[ 2] = getz(aabbs[0].min);

    vertdata[ 3] = getx(aabbs[0].max);
    vertdata[ 4] = gety(aabbs[0].min);
    vertdata[ 5] = getz(aabbs[0].min);

    vertdata[ 6] = getx(aabbs[0].min);
    vertdata[ 7] = gety(aabbs[0].max);
    vertdata[ 8] = getz(aabbs[0].min);

    vertdata[ 9] = getx(aabbs[0].max);
    vertdata[10] = gety(aabbs[0].max);
    vertdata[11] = getz(aabbs[0].min);

    vertdata[12] = getx(aabbs[0].min);
    vertdata[13] = gety(aabbs[0].min);
    vertdata[14] = getz(aabbs[0].max);

    vertdata[15] = getx(aabbs[0].max);
    vertdata[16] = gety(aabbs[0].min);
    vertdata[17] = getz(aabbs[0].max);

    vertdata[18] = getx(aabbs[0].min);
    vertdata[19] = gety(aabbs[0].max);
    vertdata[20] = getz(aabbs[0].max);

    vertdata[21] = getx(aabbs[0].max);
    vertdata[22] = gety(aabbs[0].max);
    vertdata[23] = getz(aabbs[0].max);


    inddata[0] = 0;
    inddata[1] = 1;
    inddata[2] = 0;
    inddata[3] = 2;
    inddata[4] = 1;
    inddata[5] = 3;
    inddata[6] = 2;
    inddata[7] = 3;

    inddata[ 8] = 4;
    inddata[ 9] = 5;
    inddata[10] = 4;
    inddata[11] = 6;
    inddata[12] = 5;
    inddata[13] = 7;
    inddata[14] = 6;
    inddata[15] = 7;

    inddata[16] = 0;
    inddata[17] = 4;
    inddata[18] = 1;
    inddata[19] = 5;
    inddata[20] = 2;
    inddata[21] = 6;
    inddata[22] = 3;
    inddata[23] = 7;

    MeshData mesh;
    mesh.indices = inddata;
    mesh.vertices = vertdata;
    mesh.num_indices = 24;
    mesh.num_vertices = 8;

    this->ref_aabbmesh = this->drawing->UploadMesh(DebugBox::VertexStride, &mesh);
    this->memory.SetTop(tos);

    void* top = this->memory.GetTop();
    GPUProgramLoader gpuprog_loader;
    GPUProgramData prog_data = gpuprog_loader.Load(&memory, "line_box.vs", "line_box.ps");
    this->ref_aabbgpuprog = this->drawing->UploadGPUProgram(DebugBox::VertexLayout, DebugBox::VertexElements, &prog_data);
    this->memory.SetTop(top);
}

void DebugRenderer::CreateRays(StaticGeometry* geometry, U32 vertex_number, U32 num_rays)
{
    if (ref_raymesh != REF16_INVALID)
    {
        this->drawing->FreeMesh(this->ref_raymesh);
        this->drawing->FreeGPUProgram(this->ref_raygpuprog);
    }



    Vector3 normal, position;
    geometry->GetNormals(&vertex_number, &normal, 1);
    geometry->GetPositions(&vertex_number, &position, 1);

    Vector3* dirs = (Vector3*) memory.Alloc(sizeof(Vector3) * num_rays, sizeof(Vector3));
    HemisphericalSamples(normal, num_rays, dirs);

    float* vertdata = (float*)memory.Alloc(DebugRays::VertexStride * num_rays * 2, sizeof(float));
    for (U32 i = 0; i < num_rays; ++i)
    {
        Vector3 scaled = scale(5.0, dirs[i]);
        vertdata[i * 6 + 0] = getx(position);
        vertdata[i * 6 + 1] = gety(position);
        vertdata[i * 6 + 2] = getz(position);
        vertdata[i * 6 + 3] = vertdata[i * 6 + 0] + getx(scaled);
        vertdata[i * 6 + 4] = vertdata[i * 6 + 1] + gety(scaled);
        vertdata[i * 6 + 5] = vertdata[i * 6 + 2] + getz(scaled);
    }

    U32* inddata = (U32*)memory.Alloc(sizeof(U32) * num_rays, sizeof(U32));
    for (U32 i = 0; i < num_rays; ++i)
    {
        inddata[i * 2 + 0] = i * 2;
        inddata[i * 2 + 1] = i * 2 + 1;
    }

    MeshData mesh;
    mesh.indices = inddata;
    mesh.vertices = vertdata;
    mesh.num_indices = num_rays * 2;
    mesh.num_vertices = num_rays * 2;

    this->ref_raymesh = this->drawing->UploadMesh(DebugRays::VertexStride, &mesh);

    void* top = this->memory.GetTop();
    GPUProgramLoader gpuprog_loader;
    GPUProgramData prog_data = gpuprog_loader.Load(&memory, "line_ray.vs", "line_ray.ps");
    this->ref_raygpuprog = this->drawing->UploadGPUProgram(DebugRays::VertexLayout, DebugRays::VertexElements, &prog_data);
    this->memory.SetTop(top);
}

void DebugRenderer::DrawAABBs()
{
    drawing->SetGPUProgram(this->ref_aabbgpuprog);
    drawing->DrawLines(this->ref_aabbgpuprog);
}

void DebugRenderer::DrawRays()
{
    drawing->SetGPUProgram(this->ref_raygpuprog);
    drawing->DrawLines(this->ref_raygpuprog);
}
