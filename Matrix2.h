#pragma once

#include <xmmintrin.h>
#include <immintrin.h>

using Matrix2 = __m128;
using Vector2 = __m128;

inline void invert(Matrix2* m)
{
    __m128 tmp = *m;

    float vals[4];
    _mm_store_ps(vals, tmp);

    float fdet = vals[0] * vals[3] - vals[1] * vals[2];
    __m128 determinant = _mm_set_ps1(fdet);

    __m128 sign = { 1.0f, -1.0f, -1.0f,  1.0f };
    tmp = _mm_shuffle_ps(tmp, tmp, 0b00100111);
    tmp = _mm_mul_ps(tmp, sign);
    *m = _mm_div_ps(tmp, determinant);
}

inline Vector2 mul(const Matrix2 m, const Vector2 v)
{
    __m128 doubled = _mm_movelh_ps(v, v);
    __m128 mult = _mm_mul_ps(doubled, m);
    __m128 rslt = _mm_hadd_ps(mult, mult);

    return _mm_shuffle_ps(rslt, _mm_setzero_ps(), 0b00000100);
}