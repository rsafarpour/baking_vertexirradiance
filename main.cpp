#define NOMINMAX

#include <windows.h>
#include <windowsx.h>

#include "DrawSystem.h"
#include "GPUProgramLoader.h"
#include "GeometryLoader.h"
#include "StackMemory.h"
#include "units.h"
#include "LightSolver.h"
#include "MeshComposer.h"
#include "VertexLayouts.h"
#include "DebugRenderer.h"
#include "Camera.h"
#include "sBVH.h"

Camera camera;
float yaw_angle     = 0.0f;
float pitch_angle   = 0.0f;
float x_translation = 0.0f;
float y_translation = 0.0f;
float z_translation = -2.0f;

#define ONE_DEG_AS_RAD (3.1425f/180.0f)

LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    int wheel_val = GET_WHEEL_DELTA_WPARAM(wParam);
    int mpos_x = GET_X_LPARAM(lParam);
    int mpos_y = GET_Y_LPARAM(lParam);
    switch (message)
    {
    case WM_DESTROY:
        PostQuitMessage(0);
        return 0; 
        break;
    case WM_MOUSEWHEEL:
        if (wheel_val > 0) { z_translation += 0.1f; }
        if (wheel_val < 0) { z_translation -= 0.1f; }

        z_translation = std::max(std::min(0.0f, z_translation), -10.0f);
        camera.SetPosition({x_translation, y_translation, z_translation});
        break;
    case WM_MOUSEMOVE:
        if (mpos_x < 200) { x_translation += 0.1f; }
        if (mpos_x > 600) { x_translation -= 0.1f; }
        if (mpos_y < 200) { y_translation -= 0.1f; }
        if (mpos_y > 400) { y_translation += 0.1f; }

        x_translation = std::max(std::min(5.0f, x_translation), -5.0f);
        y_translation = std::max(std::min(5.0f, y_translation), -5.0f);
        camera.SetPosition({x_translation, y_translation, z_translation});
        break;
    }

    return DefWindowProc(hWnd, message, wParam, lParam);
}

int WINAPI WinMain(HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPSTR lpCmdLine,
    int nCmdShow)
{
    WNDCLASSEX wc = {};

    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc = WindowProc;
    wc.hInstance = hInstance;
    wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
    wc.lpszClassName = L"RenderWindow";

    RegisterClassEx(&wc);
    HWND hwnd = CreateWindowEx(0,
        L"RenderWindow",
        L"RenderTest",
        WS_OVERLAPPEDWINDOW,
        300,
        300, 
        800,
        600, 
        nullptr,
        nullptr,
        hInstance,   
        nullptr);

    HeapMemory heap(13); // = 8MB

    DrawSystem drawing(&heap);
    drawing.Setup(hwnd);
    
    sBVH bvh(&heap);
    GeometryLoader geometry_loader(&heap);
    GPUProgramLoader gpuprog_loader;
    StaticGeometry geometry(&heap, &bvh);
    
    StackMemory stack(&heap, 1024 * 100);
    GPUProgramData prog_data = gpuprog_loader.Load(&stack, "shdr.vs", "shdr.ps");
    Ref16 gpu_program = drawing.UploadGPUProgram(StaticMeshes::VertexLayout, StaticMeshes::VertexElements, &prog_data);
    stack.Clear();
    
    geometry_loader.Load(&geometry, "cornell_box.mesh");
    
    LightSolver solver(&heap);
    ColorRGBA_F* irradiance = (ColorRGBA_F*)heap.Alloc(geometry.GetNumVertices() * sizeof(Vector3), 16);
    solver.SolveGeometryRadiance(&geometry, 3, irradiance);

    MeshComposer composer(&heap);
    Ref16 mesh = composer.UploadComposed(&drawing, &geometry, irradiance);
    heap.Free(irradiance);
    
    DebugRenderer dbg(&heap, &drawing);
    //dbg.CreateAABB(&bvh, 13);
    //dbg.CreateRays(&geometry, 39, 128);
    
    //--------------------

    camera.SetDrawSystem(&drawing);

    ShowWindow(hwnd, nCmdShow);

    MSG msg = {};
    while (msg.message != WM_QUIT)
    {
        PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE);
        TranslateMessage(&msg);
        DispatchMessage(&msg);
        
        float clear_color[] = {0.0, 0.0, 0.0, 1.0};
        drawing.Clear(clear_color);
        
        camera.Update();
        drawing.SetGPUProgram(gpu_program);
        drawing.DrawTriangles(mesh);
        
        //dbg.DrawAABB();
        //dbg.DrawRays();
    
        drawing.Present();
    }

    drawing.FreeMesh(mesh);
    drawing.FreeGPUProgram(gpu_program);
    camera.Destroy();

    return msg.wParam;
}