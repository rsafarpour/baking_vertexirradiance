#pragma once

#include "types.h"
#include "Color.h"

namespace MonteCarlo::Spherical
{
    ColorRGBA_F integrate(ColorRGBA_F* samples, U32 num_samples)
    {
        constexpr float pi_times_2 = 2.0f * 3.14159265359f;
        ColorRGBA_F sum = {0.0f, 0.0f, 0.0, 0.0f};

        for (U32 i = 0; i < num_samples; ++i)
        {
            sum = sum + samples[i];
        }

        ColorRGBA_F rslt = scale(pi_times_2 /(float)num_samples, sum);
        return rslt;
    }

}
