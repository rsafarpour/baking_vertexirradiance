#pragma once

#include <inttypes.h>

using U8  = uint8_t;
using U16 = uint16_t;
using U32 = uint32_t;
using U64 = uint64_t;
using I8  = int8_t;
using I16 = int16_t;
using I32 = int32_t;
using I64 = int64_t;
using IADR = U32;
using BYTE = unsigned char;

using Ref16 = U16;
using ID16  = U16;

#define REF16_INVALID 0xFFFF

