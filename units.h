#pragma once

#define FROM_KILOBYTES(kb) (1024*kb)
#define FROM_MEGABYTES(mb) (1024*1024*mb)
#define FROM_GIGABYTES(gb) (1024*1024*1024*gb)